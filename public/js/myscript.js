$("div.alert").delay(7000).slideUp();


$('.btn_pre').click(function(){
    var qty = $(this).parent().find('.btn_qty').val();
    var rowId = $(this).parent().find('.btn_qty').attr('id');
    if(qty > 1){
        qty = qty - 1;
        $(this).parent().find('.btn_qty').attr('value',qty);
        if(rowId){
            $.ajax({
                method : 'POST',
                url  :  'cart-update',
                data : {
                    qty: qty,
                    rowId: rowId
                },
                success :  function(data) {                
                    if(data=='success'){
                        location.reload();
                    }
                }
            });
        }
    }
});
$('.btn_next').click(function(){
    var qty = $(this).parent().find('.btn_qty').val();  
    var rowId = $(this).parent().find('.btn_qty').attr('id');  
    qty = parseInt(qty) + 1;
    $(this).parent().find('.btn_qty').attr('value',qty);
    if(rowId){
        $.ajax({
            method : 'POST',
            url  :  'cart-update',
            data : {
                qty: qty,
                rowId: rowId
            },
            success :  function(data) {                
                if(data=='success'){
                    location.reload();
                }
            }
        });
    }
});

$('.color-checkbox').change(function(){    
    $('.price-submit').click();
});

$('.size-checkbox').change(function(){    
    $('.price-submit').click();
});

$('.orderby').change(function(){
    $('.price-submit').click();
});

// $('.ui-slider-handle').focus(function(){
//     console.log(123);
// });

function Init_JS() {
    //Error Block
    $('div.alert').delay(5000).slideUp();

    $('.language').click(function(e){
        e.preventDefault();
        var lang = $(this).attr('data');
        var url = $(this).attr('id');

        $.ajax({
            method : 'POST',
            url  :  'language',
            data : {
                lang: lang,
                url: url
            },
            success :  function(data) {
                if(data) {
                    if(data!='fail'){
                        window.location.href = data;
                    }
                }
            }
        });
    });
}

function Init_price(){
    function showProducts(minPrice, maxPrice) {
        $("#products li").hide().filter(function() {
            var price = parseInt($(this).data("price"), 10);
            return price >= minPrice && price <= maxPrice;
        }).show();
    }

    $(function() {
        var options = {
                range: true,
                min: 0,
                max: $('#max_price').val(),
                values: $('#price_data').val().split('-'),
                slide: function(event, ui) {
                    var min = ui.values[0],
                        max = ui.values[1];

                    $("#amount").val(min + " VND" + " - " + max + " VND");
                    showProducts(min, max);
                },
                // change: function(event, ul){
                //     $('.price-submit').click(); 
                // }      
            },
            min, max;
        $("#slider-range").slider(options);
        min = $("#slider-range").slider("values", 0);
        max = $("#slider-range").slider("values", 1);
        $("#amount").val(min + " VND" + " - " + max + " VND");
        showProducts(min, max);
    });
}

$('.pop').on('click', function() {
    $('.imagepreview').attr('src', $(this).find('img').attr('src'));
    $('#imagemodal').modal('show');
});

$('.language').click(function(){
    var lang = $(this).attr('data');
    var url = $(this).attr('id');
    var link = $(this).attr('data_url');
    $.ajax({
        method : 'POST',
        url  :  link,
        data : {
            lang: lang,
            url: url
        },
        success :  function(data) {
            if(data) {
                if(data!='fail'){
                    window.location.href = data;
                }
            }
        }
    });
});
