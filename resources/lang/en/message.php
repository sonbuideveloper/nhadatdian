<?php

return array(
	'flash' => array(
		'_send-contact'	=> 'Send contact success. We are contact soon',
		'_success'		=> 'Success',
		'_error'		=> 'Error',
		'_add-cart-success'		=> 'Success | Add cart completed',
		'_order-send'			=> 'Success | Send order completed',
		'_error-email-exist' 	=> 'Error | Email exist',
		'_error-newpass'	 	=> 'Error | Please enter new password',
		'_error-oldpass'		=> 'Error | Password incorrect',
		'_change-info'			=> 'Success | Change info completed',
		'_user-active'			=> 'Success | You have successfully activate your account.',
		'_logout'				=> 'Success | Thank you',
		'_email-pass-incorrect'	=> 'Error | Email or password not correct',
		'_error-verify'			=> 'Error | Please check your email and activate your account',
		'_creat-account'		=> 'Congratulation | Account was created. Please check your email and activate your account',
		'_address-completed'	=> 'Success | Update address completed'
	)
)

?>