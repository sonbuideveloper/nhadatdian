<?php

return array(
	'url' => array(
		'_account'		=> 'account',
		'_address'		=> 'address',
		'_billing'		=> 'billing',
		'_shipping'		=> 'shipping',
		'_address-book'	=> 'address-book',
		'_edit'			=> 'edit',
		'_order'		=> 'order',
		'_category'		=> 'category'
	),
    'information' => array(
        '_avatar'    	=> 'AVATAR',
        '_firstname'   	=> 'first name',
        '_lastname'   	=> 'last name',
        '_email'   		=> 'EMAIL ADDRESS',
        '_change_password'   => 'CHANGE PASSWORD',
    	'_password'		=> 'PASSWORD',
    	'_old_pass'		=> 'old password',
    	'_new_pass'		=> 'new password',
    	'_save'			=> 'SAVE CHANGES'
    ),
	'address-book' => array(
		'_address-book'		=> 'address book',
		'_billing-address'	=> 'billing address',
		'_shipping-address'	=> 'shipping address',
		'_edit'				=> 'edit',
		'_phone'			=> 'phone',
		'_address'			=> 'address',
		'_city'				=> 'city',
		'_postal-code'		=> 'postal code',
		'_name'				=> 'name'
	),
	'orders' => array(
		'_order-no'			=> 'your order no',
		'_shipped'			=> 'shipper to',
		'_total'			=> 'total',
		'_status'			=> 'status',
		'_date'				=> 'date',
		'_view'				=> 'view order',
		'_processing'		=> 'processing',
		'_shipping'			=> 'shipping',
		'_completed'		=> 'completed',
		'_cancled'			=> 'cancled',
		'_order'			=> 'no order',
		'_no-order'			=> 'no order',
		'_payment-method'	=> 'payment method',
		'_shipping-method'	=> 'shipping method',
		'_cash'				=> 'cash',
		'_standard'			=> 'standard',
		'_order-detail'		=> 'order detail',
		'_image'			=> 'image',
		'_product'			=> 'product',
		'_price'			=> 'price',
		'_qty'				=> 'qty',
		'_subtotal'			=> 'subtotal',
		'_shipping'			=> 'shipping',
		'_total'			=> 'total'
	)
)

?>