<?php

return array(
    'search' => 'Search',
    'product_category' => 'Product categories',
    'vn' => 'Vietnamese',
    'en' => 'English',
    'my_products' => 'MY PRODUCTS',
    'contact' => 'Contact us',
    'latest_products' => 'Latest Products',
    'footer' => 'Copyright &copy; 2017. All Rights Reserved by thienminhphong.com.',
    'detail' => 'Detail',
    'thong-ke'  => 'Statistical access',
    'online'    => 'Visitors online',
    'visited'   => 'Total accessing',
    'contact-us'=> 'Get In Touch',
    'success'   => 'We\'ve received your message. We will contact you soon. Thank you!'
);

?>