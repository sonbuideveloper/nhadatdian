<?php

return array(
    'search'        => 'Tìm kiếm',
    'product_category'=> 'Danh mục sản phẩm',
    'vn'    => 'Tiếng Việt',
    'en'    => 'Tiếng Anh',
    'my_products'   => 'SẢN PHẨM CỦA CHÚNG TÔI',
    'contact'   => 'Liên hệ',
    'latest_products'   => 'Sản phẩm mới nhất',
    'footer'        => 'Bản quyền &copy; 2017 bởi thienminhphong.com.',
    'detail'        => 'Xem chi tiết',
    'thong-ke'  => 'Thống kê truy cập',
    'online'    => 'Đang online',
    'visited'   => 'Số lượt truy cập',
    'contact-us'=> 'Liên lạc với chúng tôi',
    'success'   => 'Chúng tôi đã nhận được tin nhắn của bạn. Chúng tôi sẽ liên hệ với bạn trong thời gian sớm. Cảm ơn bạn!'
);

?>