<?php

return array(
	'url' => array(
		'_account'		=> 'tai-khoan',
		'_address'		=> 'dia-chi',
		'_billing'		=> 'thanh-toan',
		'_shipping'		=> 'giao-hang',
		'_address-book'	=> 'dia-chi',
		'_edit'			=> 'sua',
		'_order'		=> 'don-hang',
		'_category'		=> 'danh-muc'
	),
    'information' => array(
        '_avatar'    	=> 'HÌNH ĐẠI DIỆN',
        '_firstname'   	=> 'tên',
        '_lastname'   	=> 'họ và tên đệm',
        '_email'   		=> 'ĐỊA CHỈ EMAIL',
        '_change_password'   => 'ĐỔI MẬT KHẨU',
    	'_password'		=> 'MẬT KHẨU',
    	'_old_pass'		=> 'mật khẩu hiện tại',
    	'_new_pass'		=> 'mật khẩu mới',
    	'_save'			=> 'LƯU THAY ĐỔI'
    ),
	'address-book' => array(
		'_address-book'		=> 'địa chỉ',
		'_billing-address'	=> 'địa chỉ thanh toán',
		'_shipping-address'	=> 'địa chỉ giao hàng',
		'_edit'				=> 'sửa',
		'_phone'			=> 'điện thoại',
		'_address'			=> 'địa chỉ',
		'_city'				=> 'thành phố',
		'_postal-code'		=> 'mã bưu chính',
		'_name'				=> 'họ tên'
	),
	'orders'	=> array(
		'_order-no'			=> 'mã đơn hàng',
		'_shipped'			=> 'vận chuyển đến',
		'_total'			=> 'tổng',
		'_status'			=> 'trạng thái',
		'_date'				=> 'ngày',
		'_view'				=> 'chi tiết',
		'_processing'		=> 'đóng gói',
		'_shipping'			=> 'vận chuyển',
		'_completed'		=> 'hoàn thành',
		'_cancled'			=> 'hủy',
		'_no-order'			=> 'bạn chưa có đơn hàng nào',
		'_payment-method'	=> 'phương thức thanh toán',
		'_shipping-method'	=> 'phương thức vận chuyển',
		'_cash'				=> 'Tiền mật',
		'_standard'			=> 'Tiêu chuẩn',
		'_order-detail'		=> 'chi tiết',
		'_image'			=> 'hình ảnh',
		'_product'			=> 'sản phẩm',
		'_price'			=> 'đơn giá',
		'_qty'				=> 'số lượng',
		'_subtotal'			=> 'giá',
		'_shipping'			=> 'vận chuyển',
		'_total'			=> 'tổng'
	)
)

?>