<?php

return array(
	'flash' => array(
		'_send-contact'	=> 'Chúng tôi đã nhận được yêu cầu của bạn. Chúng tôi sẽ sớm liên lạc với bạn.',
		'_success'		=> 'Thành công',
		'_error'		=> 'Lỗi',
		'_add-cart-success'		=> 'Chúc mừng | Sản phẩm đã được thêm vào giỏ hàng',
		'_order-send'			=> 'Chúc mừng | Yêu cầu mua hàng thành công',
		'_error-email-exist'	=> 'Lỗi | Email đã tồn tại',
		'_error-newpass'		=> 'Lỗi | Vui lòng nhập mật khẩu mới',
		'_error-oldpass'		=> 'Lỗi | Sai mật khẩu',
		'_change-info'			=> 'Chúc mừng | Thay đổi thông tin thành công',
		'_user-active'			=> 'Chúc mừng | Tài khoản của bạn đã được kích hoạt',
		'_logout'				=> 'Cảm ơn bạn đã chọn chúng tôi',
		'_email-pass-incorrect'	=> 'Lỗi | Email hoặc mật khẩu không đúng',
		'_error-verify'			=> 'Lỗi | Vui lòng kiểm tra email và kích hoạt tài khoản',
		'_creat-account'		=> 'Chúc mừng | Vui lòng kiểm tra email và kích hoạt tài khoản',
		'_address-completed'	=> 'Chúc mừng | Cập nhật địa chỉ thành công'
		
	)
)

?>