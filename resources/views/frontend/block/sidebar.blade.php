<div class="col-sm-4 col-md-3">
    <aside class="sidebar site-block">
        <!-- Search -->
        <div class="sidebar-block">
            <form action="{{ url('tim-kiem') }}" method="get">
                <div class="input-group">
                    <input type="text" id="search-term" name="k" class="form-control" placeholder="Tìm kiếm..." value="{{ isset($key)?$key:'' }}">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </form>
        </div>
        <!-- END Search -->

        <!-- Popular and Recent -->
        <div class="sidebar-block">
            <h4 class="site-heading"><strong>Tin nổi bật</strong></h4>
            <div class="tab-content">
                @if(count($featured)>0)
                    @foreach($featured as $item)
                        <div class="content-float clearfix">
                            <img src="{{ url(Storage::url($item->images)) }}" alt="Avatar" class="img-circle pull-left" style="width: 65px; margin-top: 10px;">
                            <h5><a href="{{ url('/bai-viet/'.$item->slug) }}"><strong>{{ $item->name }}</strong></a></h5>
                            <small class="text-muted">{{ date('d/m/Y', strtotime($item->created_at)) }}</small>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
        <!-- END Popular and Recent -->

        <!-- Online -->
        <?php $online = \DB::table('guest_online')->count(); ?>
        <div class="sidebar-block">
            <h4 class="site-heading"><strong>Thống kê truy cập</strong></h4>
            <p>
                Đang online: {{ $online }}<br>
                Tổng cộng: {{ $setting->shipping_fee }}
            </p>
        </div>
        <!-- END About -->
        @if($setting->adds_script)
            <div class="sidebar-block">
                {!! $setting->adds_script !!}
            </div>
        @endif
    </aside>
</div>