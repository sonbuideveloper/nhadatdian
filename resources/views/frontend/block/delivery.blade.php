<section class="background-grey p-t-40 p-b-0" style="padding-top: 10px!important; padding-bottom: 10px!important;">
    <div class="container">
        <div class="row">
            @if(count($doi_tac)>5)
                @foreach($doi_tac as $item)
                <div class="col-md-2 col-sm-4 col-xs-4 delivery">
                    <img src="{{ url(Storage::url($item->images)) }}">
                </div>
                @endforeach
            @else
                @for($i=0; $i<6; $i++)
                    <div class="col-md-2 col-sm-4 col-xs-4 delivery">
                        <img src="{{ url(Storage::url($doi_tac[$i]->images)) }}">
                    </div>
                @endfor
            @endif
        </div>
    </div>
</section>