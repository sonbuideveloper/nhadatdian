<footer class="site-footer site-section">
    <div class="container">
        <!-- Footer Links -->
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <h4 class="footer-heading">Về chúng tôi</h4>
                <ul class="footer-nav footer-nav-social list-inline">
                    <li><a href="javascript:void(0)"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="javascript:void(0)"><i class="fa fa-youtube"></i></a></li>
                    <li><a href="javascript:void(0)"><i class="fa fa-google-plus"></i></a></li>
                </ul>
            </div>
            <div class="col-sm-12 col-md-6 text-right">
                <h4 class="footer-heading" style="margin-top: 30px;">Bản Quyền Thuộc nhadatdian.org © {{ date('Y') }}</h4>
            </div>
        </div>
        <!-- END Footer Links -->
    </div>
</footer>