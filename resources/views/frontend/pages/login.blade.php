@extends('frontend.master')
@section('content')
<?php $http_url = env('HTTP_URL'); ?>
<!-- breadcrumbs area start -->
<div class="breadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="container-inner">
					<ul>
						<li class="home">
							<a href="{{ $http_url('/') }}">{{ $lang=='vn'?'Trang chủ':'Home' }}</a>
							<span><i class="fa fa-angle-right"></i></span>
						</li>
						<li class="category3"><span>{{ $lang=='vn'?'Đăng nhập':'Login' }}</span></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- breadcrumbs area end -->
<!-- account-details Area Start -->
<div class="customer-login-area">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				@if(Session::has('flash_message'))
					<div class="alert alert-{!! Session::get('flash_level') !!}">
						{!! Session::get('flash_message') !!}
					</div>
				@endif
			</div>
			<div class="col-md-6 col-xs-12">
				<div class="customer-login my-account">
					<form method="post" class="login" action="{{ $lang=='vn'?$http_url('user/login'):$http_url('en/user/login') }}">
						{!! csrf_field() !!}
						<div class="form-fields">
							<h2>{{ $lang=='vn'?'Đăng nhập':'Login' }}</h2>
							<p class="form-row form-row-wide">
								<label for="username">{{ $lang=='vn'?'Địa chỉ email':'Emall address' }} <span class="required">*</span></label>
								<input type="text" class="input-text" name="login-email" id="username" value="{{ old('login-email') }}" required>
							</p>
							<p class="form-row form-row-wide">
								<label for="password">{{ $lang=='vn'?'Mật khẩu':'Password' }} <span class="required">*</span></label>
								<input class="input-text" type="password" name="login-password" id="password" required>
							</p>
						</div>
						<div class="form-action">
							<p class="lost_password"> <a href="#">{{ $lang=='vn'?'Quên mật khẩu':'Lost your password?' }}</a></p>
							<div class="actions-log">
								<input type="submit" class="button" name="login" value="{{ $lang=='vn'?'Đăng nhập':'Login' }}">
							</div>
							<label for="rememberme" class="inline">
							<input name="login-remember-me" type="checkbox" id="rememberme" value="forever"> {{ $lang=='vn'?'Ghi nhớ':'Remember me' }} </label>
						</div>
					</form>
				</div>
			</div>
			<div class="col-md-6 col-xs-12">				
				<div class="customer-register my-account">
					<form method="post" class="register" action="{{ $lang=='vn'?$http_url('user/register'):$http_url('en/user/register') }}">
						{!! csrf_field() !!}
						<div class="form-fields">
							<h2>{{ $lang=='vn'?'Đăng ký':'Register' }}</h2>
							<p class="form-row form-row-wide">
								<label for="reg_email">{{ $lang=='vn'?'Địa chỉ email':'Email address' }} <span class="required">*</span></label>
								<input type="email" class="input-text" name="email" id="reg_email" value="" required>
							</p>
							<p class="form-row form-row-wide">
								<label for="reg_password">{{ $lang=='vn'?'Mật khẩu':'Password' }} <span class="required">*</span></label>
								<input type="password" class="input-text" name="password" id="reg_password" required>
							</p>
							<div style="left: -999em; position: absolute;">
								<label for="trap">Anti-spam</label>
								<input type="text" name="email_2" id="trap" tabindex="-1">
							</div>
						</div>
						<div class="form-action">
							<div class="actions-log">
								<input type="submit" class="button" name="register" value="{{ $lang=='vn'?'Đăng ký':'Register' }}">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- account-details Area end -->

@stop
