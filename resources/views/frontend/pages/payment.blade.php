@extends('frontend.master')
@section('content')
<?php $url = env('HTTP_URL'); ?>
    <!-- breadcrumbs area start -->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="container-inner">
                        <ul>
                            <li class="home">
                                <a href="{{ $url('/') }}">{{ $lang=='vn'?'Trang chủ':'Home' }}</a>
                                <span><i class="fa fa-angle-right"></i></span>
                            </li>
                            <li class="category3"><span>{{ $lang=='vn'?'Thanh toán':'Payment' }}</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumbs area end -->
    <!-- START MAIN CONTAINER -->
    <div class="main-container">
        <div class="product-cart" style="min-height:380px">
            <div class="container">
                <div class="row">
                    <div class="checkout-content">
                         <div class="col-md-3 category-checkout">
                        <ul>
                            <li><a class="link-hover">{{ $lang=='vn'?'Kiểm tra':'Checkout method' }}</a></li>
                            <li><a class="link-hover">{{ $lang=='vn'?'Thông tin cá nhân':'Personal Info' }}</a></li>
                            <li><a class="link-hover">{{ $lang=='vn'?'Thông tin địa chỉ':'Address Info' }}</a></li>                    
                        </ul>
                        <h5>{{ $lang=='vn'?'TIẾN HÀNH THANH TOÁN':'YOUR PAYMENT PROGRESS'}}</h5>
                        <ul>                                                                              
                            <li><a class="link-hover">{{ $lang=='vn'?'Xác nhận đơn hàng':'Confirm Order' }}</a></li>
                        </ul>
                    </div>
                        <div class="col-md-9 check-out-blok">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel checkout-accordion">
                                    <div class="panel-heading" role="tab" id="headingTwo">
                                        <h4 class="panel-title">
                                            <a class="" data-toggle="collapse" data-parent="#accordion" href="" aria-expanded="true" aria-controls="billingInformation">
                                                <span>4</span> {{ $lang=='vn'?'Phương thức thanh toán':'Payment method' }}
                                            </a>
                                        </h4>
                                    </div>
                                    <div aria-labelledby="headingTwo">
                                        <div class="content-info">
                                            <form method="post" action="payment">
                                                {!! csrf_field() !!}  
                                                <input type="hidden" name="txtUserID" value="{{ Auth::user()->id }}">                                          
                                                <div class="col-sm-12">
                                                    @if (count($errors) > 0)
                                                        <div class="alert alert-danger">
                                                            <ul>
                                                                @foreach ($errors->all() as $error)
                                                                    <li>{{ $error }}</li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    @endif

                                                    <h4 class="page-header">{{ $lang=='vn'?'Hình thức thanh toán':'Select payment methods' }}</h4>
                                                    <div class="form-group">
<!--                                                         <label>{{ $lang=='vn'?'Chọn':'Choose' }}</label> -->
                                                        {{--<div class="form-group">--}}
                                                            {{--<label class="radio-inline" for="checkout-payment-paypal">--}}
                                                                {{--<input type="radio" id="checkout-payment-paypal" name="payments" value="paypal"> <i class="fa fa-paypal"></i> Paypal--}}
                                                            {{--</label>--}}
                                                        {{--</div>--}}
                                                        <div>
                                                            <label class="radio-inline" for="checkout-payment-cash">
                                                                <input type="radio" id="checkout-payment-cash" name="payments" value="cash" checked="checked"> {{ $lang=='vn'?'Tiền mặt':'Cash' }}
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group text-right">           
                                                	@if($lang=='vn')
	                                                    <a href="{{ $url('dia-chi') }}" class="button-login-page-prev" id="pre">Quay lại</a>
	                                                    <input type="submit" class="button-login-page" id="next" value="Tiếp theo">
                                                    @else
	                                                    <a href="{{ $url('billing') }}" class="button-login-page-prev" id="pre">Previous</a>
	                                                    <input type="submit" class="button-login-page" id="next" value="Next">
                                                    @endif                                                                             
                                                </div>
                                            </form>                                
                                        </div>
                                    </div>
                                </div>                            
                            </div>
                        </div>
                    </div>
                   
                </div>
                <!-- div.info-section -->
            </div>
            <!-- Checkout Container -->
            <div class="clearfix"></div>
        </div><!-- product-cart -->
    </div>
    <!-- END MAIN CONTAINER -->
    <div class="clearfix"></div>



@stop