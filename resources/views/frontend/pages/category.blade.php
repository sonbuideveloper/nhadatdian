@extends('frontend.master')
@section('content')
    <?php $url = env('HTTP_URL'); ?>

    <section>
        <div class="container">
            <div class="row">
                <!-- Post content-->
                <div class="post-content col-md-9">
                    <div class="row m-b-20">
                        <div class="col-md-6 p-t-10 m-b-20">
                            <h3 class="m-b-20" style="text-transform: uppercase;">
                                @if($lang=='vn')
                                    {{ $category_parent->name }}
                                @else
                                    @foreach ($category_parent->metaData as $data_item)
                                        @if($data_item->language_id == $lang_id)
                                            {{ $data_item->name }}
                                        @endif
                                    @endforeach
                                @endif
                            </h3>
                        </div>
                    </div>
                    <!--Product list-->
                    <div class="shop">
                        <div class="row">
                            @foreach($products as $product)
                                <div class="col-md-4">
                                    <div class="product">
                                        @if($lang == 'vn')
                                            <div class="product-image">
                                                <a href="{{ $url('san-pham/'.$product->id.'/'.$product->slug) }}"><img style="height: 350px;" alt="Shop product image!" src="{{ $url(Storage::url($product->images)) }}"></a>
                                                <a href="{{ $url('san-pham/'.$product->id.'/'.$product->slug) }}"><img style="height: 350px;" alt="Shop product image!" src="{{ isset($product->mediadata[0]->images)?$url(Storage::url($product->mediadata[0]->images)):$url(Storage::url($product->images)) }}"></a>
                                                <div class="product-overlay">
                                                    <a href="{{ $url('san-pham/'.$product->id.'/'.$product->slug) }}">Xem chi tiết</a>
                                                </div>
                                            </div>
                                            <div class="product-description">
                                                <div class="product-category">{{ $product->categories[0]->name }}</div>
                                                <div class="product-title">
                                                    <h3><a href="{{ $url('san-pham/'.$product->id.'/'.$product->slug) }}">{{ $product->name }}</a></h3>
                                                </div>
                                                {{--<div class="product-price"><ins>{{ $product->price?$product->price:trans('home.contact') }}</ins>--}}
                                                {{--</div>--}}
                                                {{--<div class="product-rate">--}}
                                                {{--<i class="fa fa-star"></i>--}}
                                                {{--<i class="fa fa-star"></i>--}}
                                                {{--<i class="fa fa-star"></i>--}}
                                                {{--<i class="fa fa-star"></i>--}}
                                                {{--<i class="fa fa-star-half-o"></i>--}}
                                                {{--</div>--}}
                                                {{--<div class="product-reviews"><a href="#">6 customer reviews</a>--}}
                                                {{--</div>--}}
                                            </div>
                                        @else
                                            @foreach ($product->metaData as $data_item)
                                                @if($data_item->language_id == $lang_id)
                                                    <div class="product-image">
                                                        <a href="{{ $url('product/'.$product->id.'/'.$data_item->slug) }}"><img style="height: 350px;" alt="Shop product image!" src="{{ $url(Storage::url($product->images)) }}"></a>
                                                        <a href="{{ $url('product/'.$product->id.'/'.$data_item->slug) }}"><img style="height: 350px;" alt="Shop product image!" src="{{ isset($product->mediadata[0]->images)?$url(Storage::url($product->mediadata[0]->images)):$url(Storage::url($product->images)) }}"></a>
                                                        <div class="product-overlay">
                                                            <a href="{{ $url('product/'.$product->id.'/'.$data_item->slug) }}">{{ trans('home.detail') }}</a>
                                                        </div>
                                                    </div>
                                                    <div class="product-description">
                                                        @foreach ($product->categories[0]->metaData as $data_item_2)
                                                            @if($data_item_2->language_id == $lang_id)
                                                                <div class="product-category">{{ $data_item_2->name }}</div>
                                                            @endif
                                                        @endforeach
                                                        <div class="product-title">
                                                            <h3><a href="{{ $url('product/'.$product->id.'/'.$data_item->slug) }}">{{ $data_item->name }}</a></h3>
                                                        </div>
                                                        {{--<div class="product-price"><ins>{{ $product->price?$product->price:trans('home.contact') }}</ins>--}}
                                                        {{--</div>--}}
                                                        {{--<div class="product-rate">--}}
                                                        {{--<i class="fa fa-star"></i>--}}
                                                        {{--<i class="fa fa-star"></i>--}}
                                                        {{--<i class="fa fa-star"></i>--}}
                                                        {{--<i class="fa fa-star"></i>--}}
                                                        {{--<i class="fa fa-star-half-o"></i>--}}
                                                        {{--</div>--}}
                                                        {{--<div class="product-reviews"><a href="#">6 customer reviews</a>--}}
                                                        {{--</div>--}}
                                                    </div>
                                                @endif
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                    <!--END: Product list-->

                </div>
                <!-- END: Post content-->

                <!-- Sidebar-->
            @include('frontend.block.sidebar')
            <!-- END: Sidebar-->
            </div>
        </div>
    </section>

@stop