@extends('frontend.master')
@section('content')
<?php $url = env('HTTP_URL'); ?>

<div class="product-details-area">
    <div class="container">
        <div class="product-details-content">
            <div class="">
                <div class="product-details-left">
                    <div class="zoomWrapper">
                        <div id="img-1" class="zoomWrapper single-zoom">
                            @if(count($product->mediaData) > 0)
                            <a href="#">
                                <img id="zoom1" src="{{ $url('public'.Storage::url($product->mediaData[0]->images )) }}" data-zoom-image="{{ $url('public'.Storage::url($product->mediaData[0]->images)) }}" alt="big-1">
                            </a>
                            @else
                            <a href="#">
                                <img id="zoom1" src="{{ $url('frontend/img/no-image.jpg') }}" data-zoom-image="" alt="big-1">
                            </a>
                            @endif
                        </div>
                        <div class="single-zoom-thumb">
                            <ul class="bxslider" id="gallery_01">
                                @if(count($product->mediaData) > 0)
                                    @foreach($product->mediaData as $item)
                                    <li>
                                        <a href="#" class="elevatezoom-gallery active" data-update="" data-image="{{ $url('public'.Storage::url($item->images )) }}" data-zoom-image="{{ $url('public'.Storage::url($item->images )) }}"><img src="{{ $url('public'.Storage::url($item->images )) }}" alt="zo-th-1" /></a>
                                    </li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="product-details-right">
                    <div class="product-list-wrapper">
                        <div class="single-product">
                            <div class="product-content product-selection">
                                <h2 class="product-name"><a>
                                        @if($lang == 'vn')
                                            {{ $product->name }}
                                        @else
                                            @foreach ($product->metaData as $data)
                                                @if($data->language_id == $lang_id)
                                                   {{ $data->name }}
                                                @endif
                                            @endforeach
                                        @endif
                                    </a></h2>
                                <div class="rating-price">
                                    <fieldset class="rating price-rated disabled">
                                            <li {{ $rate >=1.0 ? 'class=active' : '' }}> <input class="review-big-star" type="radio" id="star5" name="rating" value="5" /><label class="full" for="star5" title="Awesome - 5 stars"></label>
                                            </li>
                                            <li {{ $rate >=2.0 ? 'class=active' : '' }}> <input class="review-big-star" type="radio" id="star4" name="rating" value="4" /><label class="full" for="star4" title="Pretty good - 4 stars"></label>
                                            </li>
                                            <li {{ $rate >=3.0 ? 'class=active' : '' }}> <input class="review-big-star" type="radio" id="star3" name="rating" value="3" /><label class="full" for="star3" title="Meh - 3 stars"></label>
                                            </li>
                                            <li {{ $rate >=4.0 ? 'class=active' : '' }}> <input class="review-big-star" type="radio" id="star2" name="rating" value="2" /><label class="full" for="star2" title="Kinda bad - 2 stars"></label>
                                            </li>
                                            <li {{ $rate >=5.0 ? 'class=active' : '' }}> <input class="review-big-star" type="radio" id="star1" name="rating" value="1" /><label class="full" for="star1" title="Sucks big time - 1 star"></label>
                                            </li>
                                    </fieldset>
                                    <div class="price-boxes">
                                        <span class="new-price">{{ number_format($product->price,0,',','.') }} VND</span>
                                    </div>
                                </div>
                                <form method="POST" action="{{ $url($lang=='vn'?'add-cart':'en/add-cart') }}">
                                    {!! csrf_field() !!}
                                    <input type="hidden" name="id" value="{{ $product->id }}">
                                    <div class="actions-e clearfix">
                                        <div class="action-buttons-single">
                                            <div class="inputx-content">
                                                <label for="qty">{{ $lang=='vn'?'Màu sắc':'color' }}</label>
                                                <!--COLOR PICKER-->
                                                    <select class="colorpicker-picker-option-selected" name="color">
                                                        @if(count($product->colors) > 0)
                                                            @foreach($product->colors as $item)
                                                                <option value="#{{ $item->value }}">{{ $item->name }}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                    <!--END COLOR PICKER-->
                                            </div>
                                            <div class="inputx-content">
                                                <label for="size">size</label>
                                                <select name="rdSize" id="size">
                                                    @if(count($product->sizes) > 0)
                                                        @foreach($product->sizes as $item)
                                                            <option value="{{ $item->name }}">{{ $item->name }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                            <div class="inputx-content">
                                                <label for="qty">{{ $lang=='vn'?'Số lượng':'quantity' }}</label>
                                                <div class="cart-product-quantity">
                                                    <div class="quantity m-l-5">
                                                        <input type="button" class="minus btn_pre" value="-">
                                                        <input type="text" class="qty btn_qty" value="1" name="txtQty">
                                                        <input type="button" class="plus btn_next" value="+">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="add-to-cart">
                                        <button type="submit">{{ $lang=='vn'?'Thêm vào giỏ hàng':'Add to cart' }}</button>
                                    </div>
                                </form>
                                <br>
                                <br>
                                <div class="product-desc">
                                    @if($lang=='vn')
                                        <h4 style="font-size:12px;color:#404040;">chi tiết sản phẩm</h4>
                                        {!! stripslashes($product->content) !!}
                                    @else
                                        <h4 style="font-size:12px;color:#404040;">product detail</h4>
                                        @foreach ($product->metaData as $data)
                                            @if($data->language_id == $lang_id)
                                                {!! stripslashes($data->content) !!}
                                            @endif
                                        @endforeach
                                    @endif
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div>
                @if($product->description)
                <div class="content_description" style="margin-top:50px; width:100%;">
                    <div class="content_img_wrapper">
                        {!! stripslashes($product->description) !!}
                    </div>
                </div>
                @endif
            </div>
        </div>

    </div>
</div>
<!-- product-details Area end -->
<!-- product section start -->
<div class="our-product-area new-product">
    <div class="" style="overflow: hidden;padding:0px 7px;">
        <div class="area-title">
            <h2>{{ $lang=='vn'?'Sản phẩm mới':'New products' }}</h2>
        </div>
        <!-- our-product area start -->
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="features-curosel">
                        @if(count($new_product) > 0)
                            @foreach($new_product as $item)
                                <div class="col-lg-12 col-md-12">
                                    <div class="single-product-new">
                                        <a href="{{ $url('product/'.$item->id.'/'.$item->slug) }}">
                                            <div class="img">
                                                @if(count($item->mediaData) > 0)
                                                    <img src="{{ $url('public'.Storage::url($item->mediaData[0]->images)) }}" alt="">
                                                @endif
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <!-- our-product area end -->
    </div>
</div>
<!-- product section end -->


<!--TAB SELECTION AREA start-->
<div class="tab-selection-area">

    <!--TAB SELECTION CONTENT-->
    <div class="tabbable">
        <ul class="nav nav-tabs product-details-tab">
            <li class="active"><a data-toggle="tab" href="#reviews">{{ $lang=='vn'?'đánh giá':'reviews' }}</a></li>
            <li class=""><a data-toggle="tab" href="#faq">faq</a></li>
            <li class=""><a data-toggle="tab" href="#shipping">{{ $lang=='vn'?'vận chuyển':'shipping' }}</a></li>
            <li class=""><a data-toggle="tab" href="#size-chart">{{ $lang=='vn'?'kích thước':'size chart' }}</a></li>
        </ul>
        <div class="tab-content">
            <!--REVIEW TAB CONTENT STAR-->
            <div id="reviews" class="tab-pane active">
                <div class="row">
                    <!--LEFT COLUMN TAB-->
                    <div class="col-md-5 col-sm-12 col-xs-12">
                        <div class="reviews-left">
                            <div class="scrollbar gray-skin" id="style-1">
                                <div class="">
                                    @if(count($product->reviews) > 0)
                                        @foreach($product->reviews as $item)
                                        <div class="block-review">
                                            <ul>
                                                @for($i=0; $i<$item->rate; $i++)
                                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                @endfor
                                            </ul>
                                            <p>{{ $item->message }}</p>
                                            <h6>&ndash; {{ $item->user->name }}</h6>
                                        </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--END LEFT COLUMN TAB-->
                    <!--RIGHT COLUMN TAB-->
                    <div class="col-md-7 col-sm-12 col-xs-12">
                        <div class="reviews-right">
                            <div class="rating-block">
                                <h6>{{ $lang=='vn'?'Xếp hạng':'rating' }}</h6>

                                <div class="rating display-rated">
                                    <span class="rating-num">{{ $rate }}</span>
                                    <div class="rating-stars">
                                        <span><i class="active icon-star"></i></span>
                                        <span><i class="active icon-star"></i></span>
                                        <span><i class="active icon-star"></i></span>
                                        <span><i class="active icon-star"></i></span>
                                        <span><i class="icon-star"></i></span>
                                    </div>
                                    <div class="rating-users">
                                        <i class="icon-user"></i>{{ count($product->reviews) ? count($product->reviews) : 0 }} {{ $lang=='vn'?'Đánh giá':'reviews' }}
                                    </div>
                                </div>

                                <div class="progress-bar-display">

                                    <div class="progress-block">
                                        <h5>5 stars</h5>
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="{{ $count_rate['5'] }}" aria-valuemin="0" aria-valuemax="100">
                                            </div>
                                        </div>
                                        <span>({{ $count_rate['5'] }})</span>
                                    </div>

                                    <div class="progress-block">
                                        <h5>4 stars</h5>
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="{{ $count_rate['4'] }}" aria-valuemin="0" aria-valuemax="100">

                                            </div>
                                        </div>
                                        <span>({{ $count_rate['4'] }})</span>
                                    </div>

                                    <div class="progress-block">
                                        <h5>3 stars</h5>
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="{{ $count_rate['3'] }}" aria-valuemin="0" aria-valuemax="100">
                                            </div>
                                        </div>
                                        <span>({{ $count_rate['3'] }})</span>
                                    </div>
                                    <div class="progress-block">
                                        <h5>2 stars</h5>
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="{{ $count_rate['2'] }}" aria-valuemin="0" aria-valuemax="100">

                                            </div>
                                        </div>
                                        <span>({{ $count_rate['2'] }})</span>
                                    </div>

                                    <div class="progress-block">
                                        <h5>1 stars</h5>
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="{{ $count_rate['1'] }}" aria-valuemin="0" aria-valuemax="124">

                                            </div>
                                        </div>
                                        <span>({{ $count_rate['1'] }})</span>
                                    </div>

                                </div>

                            </div>


                            <div class="share-review-block ">
                                <h6>{{ $lang=='vn'?'chia sẻ đánh giá':'share a review' }} </h6>
                                @if(Auth::user())
                                <form method="post" action="{{ $lang=='vn' ? $url('post-review') : $url('en/post-review') }}">
                                    {!! csrf_field() !!}
                                    <input type="hidden" name="product_id" value="{{ $product->id }}">
                                    <p>{{ $lang=='vn'?'Xin vui lòng điền vào mẫu dưới đây. Các vị trí bắt buộc được đánh dấu':'Please fill the form below. Required fields are marked' }} *</p>
                                    <p>{{ $lang=='vn'?'Đánh giá của bạn':'Your Rating' }}(*)</p>
                                    <!--FIELD SET STAR RATING STAR-->
                                    <fieldset class="rating-small review-rated">
                                        @if(isset($review))
                                            <input {{ $review->rate == 5 ? 'checked="checked"' : '' }} type="radio" id="star_5" name="rating" value="5" /><label class="full" for="star_5" title="Awesome - 5 stars"></label>
                                            <input {{ $review->rate == 4 ? 'checked="checked"' : '' }} type="radio" id="star_4" name="rating" value="4" /><label class="full" for="star_4" title="Pretty good - 4 stars"></label>
                                            <input {{ $review->rate == 3 ? 'checked="checked"' : '' }} type="radio" id="star_3" name="rating" value="3" /><label class="full" for="star_3" title="Meh - 3 stars"></label>
                                            <input {{ $review->rate == 2 ? 'checked="checked"' : '' }} type="radio" id="star_2" name="rating" value="2" /><label class="full" for="star_2" title="Kinda bad - 2 stars"></label>
                                            <input {{ $review->rate == 1 ? 'checked="checked"' : '' }} type="radio" id="star_1" name="rating" value="1" /><label class="full" for="star_1" title="Sucks big time - 1 star"></label>
                                        @else
                                            <input type="radio" id="star_5" name="rating" value="5" /><label class="full" for="star_5" title="Awesome - 5 stars"></label>
                                            <input type="radio" id="star_4" name="rating" value="4" /><label class="full" for="star_4" title="Pretty good - 4 stars"></label>
                                            <input type="radio" id="star_3" name="rating" value="3" /><label class="full" for="star_3" title="Meh - 3 stars"></label>
                                            <input type="radio" id="star_2" name="rating" value="2" /><label class="full" for="star_2" title="Kinda bad - 2 stars"></label>
                                            <input type="radio" id="star_1" name="rating" value="1" /><label class="full" for="star_1" title="Sucks big time - 1 star"></label>
                                        @endif
                                    </fieldset>
                                    <!--END FIELD SET STAR RATING-->


                                    <p>{{ $lang=='vn'?'Nội dung':'Message' }}(*)</p>
                                    @if(isset($review))
                                        <textarea name="message">{{ $review->message ? $review->message : old('message') }}</textarea>
                                    @else
                                        <textarea name="message"></textarea>
                                    @endif
                                    <div class="submit-button ">
                                        <button type="submit">{{ $lang=='vn'?'GỬI':'SUBMIT' }}</button>
                                    </div>
                                </form>
                                @else
                                <a href="{{ $url('users/login') }}"><p>
                                        @if($lang=='vn')
                                            Vui lòng nhấn vào đây để đăng nhập hoặc đăng ký.
                                        @else
                                            Please click here to login or register.
                                        @endif
                                    </p></a>
                                @endif
                            </div>
                        </div>

                    </div>
                    <!--END RIGHT COLUMN TAB-->
                </div>
            </div>
            <!--REVIEW TAB CONTENT END-->

            <div id="faq" class="tab-pane">
                <div class="faq-content">
                    @if($lang=='vn')
                        {!! $faq ? stripslashes($faq->content) : '' !!}
                    @else
                        @foreach ($faq->metaData as $data)
                            @if($data->language_id == $lang_id)
                                {!! $data ? stripslashes($data->content) : '' !!}
                            @endif
                        @endforeach
                    @endif
                </div>

            </div>
            <div id="shipping" class="tab-pane ">
                <div class="shipping-content">
                    @if($lang=='vn')
                        {!! $shipping ? stripcslashes($shipping->content) : '' !!}
                    @else
                        @foreach ($shipping->metaData as $data)
                            @if($data->language_id == $lang_id)
                                {!! $data ? stripcslashes($data->content) : '' !!}
                            @endif
                        @endforeach
                    @endif
                </div>

            </div>
            <div id="size-chart" class="tab-pane ">
                    <table class="table" style="overflow:auto">
                        <div class="" style="overflow:auto;">
                            <tbody>
                                <tr class="tab-head">
                                    <td>sizes</td>
                                    <td>uk</td>
                                    <td>us</td>
                                    <td>eu</td>
                                    <td>bust</td>
                                    <td>waist</td>
                                    <td>hip</td>

                                </tr>
                                @if($size)
                                    @foreach($size as $item)
                                    <tr>
                                        <td>{{ $item->name }}</td>
                                        @if($item->content)
                                            @foreach(explode('---', strip_tags($item->content,'p')) as $value)
                                                <td>{{ $value }}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </div>
                    </table>
                </div>
        </div>
    </div>
    <!--TAB SELECTION CONTENT end-->


</div>



@stop
