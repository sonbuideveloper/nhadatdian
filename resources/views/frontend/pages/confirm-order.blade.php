@extends('frontend.master')
@section('content')
<?php $url = env('HTTP_URL'); ?>
<style>
    tr.price td{
        padding: 10px !important;
    }

    tr.price td div{
        font-size: 15px !important;
    }

</style>
    <!-- breadcrumbs area start -->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="container-inner">
                        <ul>
                            <li class="home">
                                <a href="{{ $url('/') }}">{{ $lang=='vn'?'Trang chủ':'Home' }}</a>
                                <span><i class="fa fa-angle-right"></i></span>
                            </li>
                            <li class="category3"><span>{{ $lang=='vn'?'Xác nhận đơn hàng':'Confirm Order' }}</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumbs area end -->
    <!-- START MAIN CONTAINER -->
    <div class="main-container">
        <div class="product-cart">
            <div class="container">
                <div class="row">
                    <div class="checkout-content">
                        <div class="col-md-3 category-checkout">
                        <ul>
                            <li><a class="link-hover">{{ $lang=='vn'?'Kiểm tra':'Checkout method' }}</a></li>
                            <li><a class="link-hover">{{ $lang=='vn'?'Thông tin cá nhân':'Personal Info' }}</a></li>
                            <li><a class="link-hover">{{ $lang=='vn'?'Thông tin địa chỉ':'Address Info' }}</a></li>
                            <li><a class="link-hover">{{ $lang=='vn'?'Phương thức thanh toán':'Payment Method' }}</a></li>
                        </ul>
                        <h5>{{ $lang=='vn'?'XÁC NHẬN ĐƠN HÀNG':'CONFIRM ORDER'}}</h5>
                    </div>
                        <div class="col-md-9 check-out-blok">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel checkout-accordion">
                                    <div class="panel-heading" role="tab" id="headingTwo">
                                        <h4 class="panel-title">
                                            <a class="" data-toggle="collapse" data-parent="#accordion" href="" aria-expanded="true" aria-controls="billingInformation">
                                                <span>5</span> {{ $lang=='vn'?'Xác nhận đơn hàng':'Confirm Order' }}
                                            </a>
                                        </h4>
                                    </div>
                                    <div aria-labelledby="headingTwo">
                                        <div class="content-info">
                                            <form method="post" action="confirm-order">
                                                {!! csrf_field() !!}
                                                <input type="hidden" name="txtUserID" value="{{ Auth::user()->id }}">
                                                <div class="row">
                                                <div class="col-sm-12">
                                                    @if (count($errors) > 0)
                                                        <div class="alert alert-danger">
                                                            <ul>
                                                                @foreach ($errors->all() as $error)
                                                                    <li>{{ $error }}</li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    @endif

                                                <div style="overflow:auto">
                                                    <table class="cart-table" style="margin-bottom: 0px;">
                                                        <thead>
                                                        <tr>
                                                        	@if($lang=='vn')
                                                            	<th>Hình ảnh</th>
                                                            	<th>Thông tin</th>
                                                            	<th>Đơn giá</th>
                                                            	<th>Số lượng</th>
                                                            	<th>Giá</th>
                                                            @else
                                                            	<th>Images</th>
                                                            	<th>Product Details</th>
                                                            	<th>Unit Price</th>
                                                            	<th>Qty</th>
                                                            	<th>Subtotal</th>
                                                            @endif
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($product as $item)
                                                            <tr>
                                                                <td>
                                                                    <a href="{{ $url('public'.Storage::url($item->options->img)) }}"><img src="{{ $url('public'.Storage::url($item->options->img)) }}" class="img-responsive" alt="" style="width: 80px;max-width:none; margin-left: auto; margin-right: auto;" /></a>
                                                                </td>
                                                                <td>
                                                                    <h6>{{ $item->name }}</h6>
                                                                    <br>
                                                                    <p>Size: {{ $item->options->size }}</p>
                                                                    <p>{{ $lang=='vn'?'Màu sắc':'Color' }}: <span style="background-color: {{ $item->options->color }}; min-width: 20px; display: inline-block;">&nbsp;</span></p>
                                                                </td>
                                                                <td>
                                                                    <div class="cart-price">{{ number_format($item->price,0,',','.') }} VND</div>
                                                                </td>
                                                                <td>
                                                                    <div class="input-qty" >{!! $item->qty !!}</div>
                                                                </td>
                                                                <td>
                                                                    <div class="cart-subtotal">{!! number_format(($item->price * $item->qty),0,',','.') !!} VND</div>
                                                                </td>
                                                            </tr>
                                                            @endforeach
                                                            <tr class="price">
                                                                <td colspan="3"></td>
                                                                <td><div class="cart-subtotal">{{ $lang=='vn'?'Tạm tính':'Sub Total' }}: </div></td>
                                                                <td><div class="cart-subtotal">{{ Cart::subtotal(0,',','.') }} VND</div></td>
                                                            </tr>
                                                            <tr class="price">
                                                                <td colspan="3"></td>
                                                                <td><div class="cart-subtotal">VAT({{ $setting->VAT }}%): </div></td>
                                                                <td><div class="cart-subtotal">{{ number_format(Cart::subtotal(0,',','')*$setting->VAT/100,0,',','.') }} VND</div></td>
                                                            </tr>
                                                            @if($setting->shipping_fee > 0)
                                                            <tr class="price">
                                                                <td colspan="3"></td>
                                                                <td><div class="cart-subtotal">{{ $lang=='vn'?'Phí vận chuyển':'Shipping Cost' }}: </div></td>
                                                                <td><div class="cart-subtotal">{{ number_format($setting->shipping_fee,0,',','.') }} VND</div></td>
                                                            </tr>
                                                            @endif
                                                            <tr class="price">
                                                                <td colspan="3"></td>
                                                                <td><div class="cart-subtotal">{{ $lang=='vn'?'TỔNG TIỀN':'TOTAL PRICE' }}: </div></td>
                                                                <td><div class="cart-subtotal" style="background-color: #f0f0f0;min-width:120px"><input type="text" readonly name="total_price" value="{{ number_format(Cart::subtotal(0,',','') + Cart::subtotal(0,',','')*$setting->VAT/100 + $setting->shipping_fee ,0,',','.') }} VND"></div></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                            </div>
                                                    <div class="col-sm-6">
                                                         <div class="account-title-block">
                                                        <h6 style="margin-top:40px">{{ $lang=='vn'?'Địa chỉ thanh toán':'Billing Address' }}</h6>
                                                         </div>
                                                        <p style="text-transform:uppercase">{{ Auth::user()->name }}</p>

                                                        <address>
                                                            <p>{{ $lang=='vn'?'Địa chỉ':'Address' }}: {{ $billing_data['address'] }}</p>
                                                            <p>{{ $lang=='vn'?'Số điện thoại':'Phone' }}: {{ $billing_data['phone'] }}</p>
                                                            <p>{{ $lang=='vn'?'Mã bưu chính':'Postal code' }}: {{ $billing_data['postal_code'] }}</p>
                                                            <p>{{ $lang=='vn'?'Thành phố':'City' }}: {{ $billing_data['city'] }}</p>
                                                        </address>
                                                    </div>
                                                    <div class="col-sm-6">
                                                      <div class="account-title-block">
                                                        <h6 style="margin-top:40px">{{ $lang=='vn'?'Địa chỉ giao hàng':'Shipping Address' }}</h6>
                                                        <p style="text-transform:uppercase">{{ $shipping_data['name'] }}</p>
                                                        <address>
                                                            <p>{{ $lang=='vn'?'Địa chỉ':'Address' }}: {{ $shipping_data['address'] }}</p>
                                                            <p>{{ $lang=='vn'?'Số điện thoại':'Phone' }}: {{ $shipping_data['phone'] }}</p>
                                                            <p>{{ $lang=='vn'?'Mã bưu chính':'Postal code' }}: {{ $shipping_data['postal_code'] }}</p>
                                                            <p>{{ $lang=='vn'?'Thành phố':'City' }}: {{ $shipping_data['city'] }}</p>
                                                        </address>
                                                    </div>
                                                </div>
                                                </div>
                                                <div class="form-group text-right">
                                                	@if($lang=='vn')
	                                                    <a href="{{ $url('thanh-toan') }}" class="button-login-page-prev" id="pre">Quay lại</a>
	                                                    <input type="submit" class="button-login-page" style="width:150px;padding-left:2px" id="next" value="Xác nhận đơn hàng">
                                                    @else
	                                                    <a href="{{ $url('payment') }}" class="button-login-page-prev" id="pre">Previous</a>
                                                    <input type="submit" class="button-login-page" style="width:150px;padding-left:2px" id="next" value="Confirm Order">
                                                    @endif
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- div.info-section -->
            </div>
            <!-- Checkout Container -->
            <div class="clearfix"></div>
        </div><!-- product-cart -->
    </div>
    <!-- END MAIN CONTAINER -->
    <div class="clearfix"></div>



@stop
