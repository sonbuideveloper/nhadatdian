@extends('frontend.master')
@section('content')
    <style>
        .site-block .content{
            text-align: center;
        }
        .site-block img{
            max-width: 80%;
            display: block;
            margin: 0 auto;
        }
        .site-block p{
            font-size: 15px;
        }
        .site-block h1{
            font-size: 25px;
            font-weight: bold;
            text-transform: uppercase;
            border-bottom: 1px solid #eeeeee;
            margin-bottom: 30px;
            text-align: justify;
        }

        @media (max-width: 991px){
            .site-block img{
                max-width: 90%;
                height: 100% !important;
            }
        }
    </style>
    <!-- Article -->
    <section class="site-content site-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-md-9 site-block">
                    <!-- Story -->
                    <div class="content">
                        <h1>{{ $article->name }}</h1>
                        <article>
                            {!! stripcslashes($article->content) !!}
                        </article>
                        <!-- END Story -->
                    </div>

                    {{--@if($setting->adds_script)--}}
                        {{--<div style="margin-bottom: 30px;">--}}
                            {{--{!! $setting->adds_script !!}--}}
                        {{--</div>--}}
                    {{--@endif--}}

                    <div style="margin-left: 15px;">
                        <div class="fb-like" data-href="{{ url('bai-viet/'.$article->slug) }}" data-width="500" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
                        <div class="fb-comments" data-href="{{ url('bai-viet/'.$article->slug) }}" data-width="100%" data-numposts="10"></div>
                    </div>
                </div>

                <!-- Sidebar -->
                @include('frontend.block.sidebar')
                <!-- END Sidebar -->
            </div>
            <hr>
        </div>
    </section>
    <!-- END Article -->

@stop