@extends('frontend.master')
@section('content')
<?php $url = env('HTTP_URL'); ?>
<!--account area start-->
<div class="account-area">
    <div class="container">
        <div class="row">
            <!-- left sidebar start -->
            <div class="col-md-3 col-sm-4 col-xs-12">
                <aside class="sidebar-content account-nav">
                    <div class="sidebar-title-account">
                        <h6>{{ $lang=='vn'?'TÀI KHOẢN':'MY ACCOUNT' }}</h6>
                    </div>
                    @if($lang=='vn')
                    <ul class="sidebar-tags-account">
                        <li><a href="{{ $url('tai-khoan/bang-dieu-khien') }}" class="active">. quản lý tài khoản</a></li>
                        <li><a href="{{ $url('tai-khoan/thong-tin') }}">thông tin tài khoản</a></li>
                        <li><a href="{{ $url('tai-khoan/dia-chi') }}">địa chỉ</a></li>
                        <li><a href="{{ $url('tai-khoan/don-hang') }}">thông tin đơn hàng</a></li>
                    </ul>
                    @else
                   	<ul class="sidebar-tags-account">
                        <li><a href="{{ $url('account/dashboard') }}" class="active">. account dashboard</a></li>
                        <li><a href="{{ $url('account/information') }}">account information</a></li>
                        <li><a href="{{ $url('account/address-book') }}">address book</a></li>
                        <li><a href="{{ $url('account/orders') }}">my orders</a></li>
                    </ul>
                    @endif
                </aside>
            </div>
            <!--left sidebar end -->

            <!--account right content start-->
            <div class="col-md-9 col-sm-8 col-xs-12">
                <div class="right-account-content">
                    <div class="intro-right-account-content">
                        <p>{{ $lang=='vn'?'Chào mừng':'Hello' }}, {{ Auth::user()->name }}</p>
                        @if(Auth::user()->images)
                            <a class="pop"><img src="{{ $url('public'.Storage::url(Auth::user()->images)) }}" alt="" style="width: 150px;"></a>
                        @else
                            <a class="pop"><img src="{{ $url('frontend/img/no-image.jpg') }}" alt="" style="width: 150px;"></a>
                        @endif
                        <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                        <img src="" class="imagepreview">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--BLOCK CONTENT start-->
                    <div class="account-block-content">
                        <div class="account-title-block">                        
                        	@if($lang=='vn')
                            	<h6>ĐƠN HÀNG GẦN ĐÂY |</h6>
                            	<a href="{{ $url('tai-khoan/don-hang') }}"><span>XEM TẤT CẢ</span></a>
                            @else
                            	<h6>RECENT ORDERS |</h6>
                            	<a href="{{ $url('account/orders') }}"><span>VIEW ALL</span></a>
                            @endif
                        </div>
                        <!--TABLE order start-->
                        <div class="table-account-order">
                            @if(count($orders) > 0)
                                <table class="table">
                                    <tr>
                                    	@if($lang=='vn')
	                                        <td>MÃ ĐƠN HÀNG.</td>
	                                        <td>VẬN CHUYỂN ĐẾN</td>
	                                        <td>TỔNG</td>
	                                        <td>TRẠNG THÁI</td>
	                                        <td>NGÀY</td>
	                                        <td></td>
                                        @else
                                        	<td>YOUR ORDER NO.</td>
	                                        <td>SHIPPED TO</td>
	                                        <td>TOTAL</td>
	                                        <td>STATUS</td>
	                                        <td>DATE</td>
	                                        <td></td>
                                        @endif
                                    </tr>
                                    @foreach($orders as $item)
                                        <tr>
                                            <td>OID.{{ $item->id }}</td>
                                            <td>{{ $item->shippingData->name }}</td>
                                            <td>{{ number_format($item->price,0,',','.') }} VND</td>                                            
                                            <td>
                                            <?php 
                                            	if($lang=='vn'){
	                                                switch ($item->publish) {
	                                                    case 1:
	                                                        echo '<span class="text-primary">Đóng gói</span>';
	                                                        break;
	                                                    case 2:
	                                                        echo '<span class="text-muted">Vận chuyển</span>';                                                        
	                                                        break;
	                                                    case 3:
	                                                        echo '<span class="text-success">Hoàn thành</span>';                                                        
	                                                        break;
	                                                    case 4:
	                                                        echo '<span class="text-danger">Hủy</span>';                                                       
	                                                        break;
	                                                    default:                                                                                                            
	                                                        break;
	                                                }
                                            	}else{
                                            		switch ($item->publish) {
                                            			case 1:
                                            				echo '<span class="text-primary">Packaging</span>';
                                            				break;
                                            			case 2:
                                            				echo '<span class="text-muted">Delivery</span>';
                                            				break;
                                            			case 3:
                                            				echo '<span class="text-success">Completed</span>';
                                            				break;
                                            			case 4:
                                            				echo '<span class="text-danger">Canceled</span>';
                                            				break;
                                            			default:
                                            				break;
                                            		}
                                            	}
                                            ?>  
                                            </td>                                          
                                            <td>{{ convertDateTime($item->created_at) }}</td>
                                            @if($lang=='vn')
                                            	<td class="link-to-view"><a href="{{ $url('tai-khoan/don-hang/'.$item->id) }}">Chi tiết</a></td>
                                            @else
                                            	<td class="link-to-view"><a href="{{ $url('account/order/'.$item->id) }}">View order</a></td>
                                            @endif
                                        </tr>
                                    @endforeach
                                </table>
                            @endif
                        </div>
                        <!--TABLE ORDER end-->
                    </div>
                    <!--BLOCK CONTENT end-->
					@if($lang=='vn')
                    <div class="account-block-content">
                        <div class="account-title-block">
                            <h6>THÔNG TIN TÀI KHOẢN |</h6>
                            <a href="{{ $url('tai-khoan/thong-tin') }}"><span>SỬA</span></a>
                        </div>
                        <div class="account-info-content">
                            <p>{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</p>
                            <span>{{ Auth::user()->email }}</span>
                        </div>
                    </div>
                    @else
                    <div class="account-block-content">
                        <div class="account-title-block">
                            <h6>ACCOUNT INFORMATION |</h6>
                            <a href="{{ $url('account/information') }}"><span>EDIT</span></a>
                        </div>
                        <div class="account-info-content">
                            <p>{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</p>
                            <span>{{ Auth::user()->email }}</span>
                        </div>
                    </div>
                    @endif
					
					@if($lang=='vn')
                    <div class="account-block-content">
                        <div class="account-title-block">
                            <h6>ĐỊA CHỈ |</h6>
                            <a href="{{ $url('tai-khoan/dia-chi') }}"><span>QUẢN LÝ ĐỊA CHỈ</span></a>
                            <div class="address-info-content">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="address-detail-content">
                                            <div class="address-edit-block">
                                                <h6>ĐỊA CHỈ THANH TOÁN</h6>
                                                
                                            </div>
                                            @if($billing_data)
                                                <p style="text-transform: uppercase;">{{ Auth::user()->name }}</p>
                                                <p>Điện thoại: {{ $billing_data->phone }}</p>
                                                <p>Địa chỉ: {{ $billing_data->address }}</p>
                                                <p>Thành phố: {{ $billing_data->city }}</p>
                                                <p>Mã bưu chính: {{ $billing_data->postal_code }}</p>                                            
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="address-detail-content">
                                            <div class="address-edit-block">
                                                <h6>ĐỊA CHỈ GIAO HÀNG</h6>
                                                
                                            </div>
                                            @if($shipping_data)
                                                <p style="text-transform: uppercase;">{{ $shipping_data->name }}</p>
                                                <p>Điện thoại: {{ $shipping_data->phone }}</p>
                                                <p>Địa chỉ: {{ $shipping_data->address }}</p>
                                                <p>Thành phố: {{ $shipping_data->city }}</p>
                                                <p>Mã bưu chính: {{ $shipping_data->postal_code }}</p>  
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @else
                    <div class="account-block-content">
                        <div class="account-title-block">
                            <h6>ADDRESS BOOK |</h6>
                            <a href="{{ $url('account/address-book') }}"><span>MANAGE ADDRESSES</span></a>
                            <div class="address-info-content">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="address-detail-content">
                                            <div class="address-edit-block">
                                                <h6>BILLING ADDRESS</h6>
                                                
                                            </div>
                                            @if($billing_data)
                                                <p style="text-transform: uppercase;">{{ Auth::user()->name }}</p>
                                                <p>Phone: {{ $billing_data->phone }}</p>
                                                <p>Address: {{ $billing_data->address }}</p>
                                                <p>City: {{ $billing_data->city }}</p>
                                                <p>Postal Code: {{ $billing_data->postal_code }}</p>                                            
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="address-detail-content">
                                            <div class="address-edit-block">
                                                <h6>SHIPPING ADDRESS</h6>
                                                
                                            </div>
                                            @if($shipping_data)
                                                <p style="text-transform: uppercase;">{{ $shipping_data->name }}</p>
                                                <p>Phone: {{ $shipping_data->phone }}</p>
                                                <p>Address: {{ $shipping_data->address }}</p>
                                                <p>City: {{ $shipping_data->city }}</p>
                                                <p>Postal Code: {{ $shipping_data->postal_code }}</p>  
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
            <!-- account right content end -->
        </div>
    </div>
</div>
<!--account area end-->

@stop