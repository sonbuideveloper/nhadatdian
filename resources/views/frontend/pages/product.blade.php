@extends('frontend.master')
@section('content')
<?php $url = env('HTTP_URL'); ?>

<section id="product-page" class="product-page p-b-0">
    <div class="container">
        <div class="row">
            <!-- Post content -->
            <div class="post-content product col-md-9">
                <div class="row">
                    <div class="col-md-5">
                        <div class="product-image">
                            <div class="carousel" data-carousel-col="1" data-lightbox-type="gallery">
                                <a href="{{ url(Storage::url($product->images)) }}" title="Shop product image!"><img alt="Shop product image!" src="{{ url(Storage::url($product->images)) }}"></a>
                                @if(isset($product->mediaData) && count($product->mediaData)>0)
                                    @foreach($product->mediaData as $item)
                                        <a href="{{ url(Storage::url($item->images)) }}" title="Shop product image!"><img alt="Shop product image!" src="{{ url(Storage::url($item->images)) }}"></a>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>


                    <div class="col-md-7">
                        @if($lang == 'vn')
                            <div class="product-description">
                                <div class="product-category">{{ $product->categories[0]->name }}</div>
                                <div class="product-title">
                                    <h3><a>{{ $product->name }}</a></h3>
                                    <br>
                                </div>
                                {!! $product->content !!}
                            </div>
                        @else
                            @foreach ($product->metaData as $data_item)
                                @if($data_item->language_id == $lang_id)
                                    <div class="product-description">
                                        <div class="product-category">
                                            @foreach ($product->categories[0]->metaData as $data_item_2)
                                                @if($data_item_2->language_id == $lang_id)
                                                    {{ $data_item_2->name }}
                                                @endif
                                            @endforeach
                                        </div>
                                        <div class="product-title">
                                            <h3><a>{{ $data_item->name }}</a></h3>
                                            <br>
                                        </div>
                                        {!! $data_item->content !!}
                                    </div>
                                @endif
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="row m-t-40">
                    <div class="col-md-12">
                        <div class="fb-comments" data-href="{{ url('/') }}" data-numposts="5"></div>
                    </div>
                </div>
            </div>
            <!-- Post content -->

            <!-- Sidebar-->
            @include('frontend.block.sidebar')
            <!-- END: Sidebar-->

        </div>
    </div>
</section>
@stop
