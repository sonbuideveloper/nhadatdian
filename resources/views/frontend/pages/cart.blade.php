@extends('frontend.master')
@section('content')
<?php $http_url = env('HTTP_URL'); ?>
    <!-- breadcrumbs area start -->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="container-inner">
                        <ul>
                            <li class="home">
                                <a href="{{ $http_url('/') }}">{{ $lang=='vn'?'Trang chủ':'Home' }}</a>
                                <span><i class="fa fa-angle-right"></i></span>
                            </li>
                            <li class="category3"><span>{{ $lang=='vn'?'Giỏ hàng':'Shopping Cart' }}</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumbs area end -->
    <!-- Shopping Table Container -->
    <div class="cart-area-start">
        <div class="container">
            <!-- Shopping Cart Table -->
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">

                            {!! csrf_field() !!}
                            <table class="cart-table">
                                <thead>
                                    <tr>
                                        <th>{{ $lang=='vn'?'Hình ảnh':'Images' }}</th>
                                        <th>{{ $lang=='vn'?'Thông tin chi tiết':'Product Details' }}</th>
                                        <th>{{ $lang=='vn'?'Đơn giá':'Unit Price' }}</th>
                                        <th>{{ $lang=='vn'?'Số lượng':'Qty' }}</th>
                                        <th>{{ $lang=='vn'?'Giá':'Subtotal' }}</th>
                                        <th>{{ $lang=='vn'?'Xóa':'Delete' }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($product as $item)
                                    <tr>
                                        <td>
                                            <a href="{{ $http_url('public'.Storage::url($item->options->img)) }}"><img src="{{ $http_url('public'.Storage::url($item->options->img)) }}" class="img-responsive" alt="" style="width: 80px;max-width:none; margin-left: auto; margin-right: auto;" /></a>
                                        </td>
                                        <td>
                                            <h6>{{ $item->name }}</h6>
                                            <br>
                                            <p>Size: {{ $item->options->size }}</p>
                                            <p>{{ $lang=='vn'?'Màu sắc':'Color' }}: <span style="background-color: {{ $item->options->color }}; min-width: 20px; display: inline-block;">&nbsp;</span></p>
                                        </td>
                                        <td>
                                            <div class="cart-price">{{ number_format($item->price,0,',','.') }} VND</div>
                                        </td>
                                        <td>
                                            <div class="button_checkbox" style="position:relative;">
                                                <input type="button" class="plus button-cart btn_next" value="+" style="width: 25px; display:block;padding:4px 6px;">
                                                <input readonly type="text" class="qty btn_qty" value="{!! $item->qty !!}" style="width: 80px" name="txtQty" id="{{ $item->rowId }}">
                                                <input type="button" class="minus button-cart btn_pre" value="-" style="width: 25px; display:block;padding:4px 6px;">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="cart-subtotal">{!! number_format(($item->price * $item->qty),0,',','.') !!} VND</div>
                                        </td>
                                        <td>
                                            <a href="{{ $http_url('cart-delete/'.$item->rowId) }}"><i class="fa fa-times"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    
                                    <tr class="hidden-sm hidden-xs">
										<td class="actions-crt" colspan="7">
											<div class="">
												<div class="col-md-4 col-sm-4 col-xs-4 align-left"><a class="cbtn" href="{{ $http_url('/') }}">{{ $lang=='vn'?'TIẾP TỤC MUA HÀNG':'CONTINUE SHOPPING' }}</a></div>
												<div class="col-md-4 col-sm-4 col-xs-4 col-xs-push-3 col-md-push-4 col-sm-push-3 align-right"><a class="cbtn" href="{{ $http_url('clear-cart') }}">{{ $lang=='vn'?'XÓA GIỎ HÀNG':'CLEAR SHOPPING CART' }}</a></div>
											</div>
										</td>
									</tr>
                                </tbody>
                            </table>
                  
                    </div>
                      <div class="col-md-6 col-sm-6 col-xs-12 align-left visible-sm visible-xs">
                        <div class="cart-button-holder">
                            <a class="cbtn" href="{{ $http_url('/') }}">{{ $lang=='vn'?'TIẾP TỤC MUA HÀNG':'CONTINUE SHOPPING' }}</a>
                        </div>
                    </div>
                   
               
                    <div class="col-md-6 col-sm-6 col-xs-12 align-right visible-sm visible-xs">
                        <div class="cart-button-holder">
                            <a class="cbtn" href="{{ $http_url('clear-cart') }}">{{ $lang=='vn'?'XÓA GIỎ HÀNG':'CLEAR SHOPPING CART' }}</a>
                        </div>
                    </div>
                </div>
              
                  

            </div>
            <!-- Shopping Cart Table -->
            <!-- Shopping Coupon -->
            <div class="row">
                <div class="col-md-12 vendee-clue">
                    <!-- Shopping Totals -->
                    <div class="shipping coupon cart-totals" style="float: right;">
                        <ul>
                            <li class="cartSubT">{{ $lang=='vn'?'Tạm tính':'Subtotal' }}:    <span>{{ Cart::subtotal(0,',','.') }} VND</span></li>
                            <li class="cartSubT">VAT({{ $setting->VAT }}%):    <span>{{ number_format(Cart::subtotal(0,',','')*$setting->VAT/100,0,',','.') }} VND</span></li>
                            <li class="cartSubT">{{ $lang=='vn'?'Tổng tiền':'Grand total' }}:   <span>{{ number_format(Cart::subtotal(0,',','') + Cart::subtotal(0,',','')*$setting->VAT/100,0,',','.') }} VND</span></li>
                        </ul>
                        @if(Cart::subtotal() > 0)
                            <a class="proceedbtn" href="{{ $http_url($lang=='vn'?'kiem-tra':'checkout') }}">{{ $lang=='vn'?'TIẾN HÀNH THANH TOÁN':'PROCEED TO CHECK OUT' }}</a>
                        @endif
                    </div>
                    <!-- Shopping Totals -->
                </div>
            </div>
            <!-- Shopping Coupon -->
        </div>
    </div>
    <!-- Shopping Table Container -->



@stop
