@extends('frontend.master')
@section('content')
<?php $url = env('HTTP_URL'); ?>
<!--account area start-->
<div class="account-area">
    <div class="container">
        <div class="row">
            <!-- left sidebar start -->
            <div class="col-md-3 col-sm-4 col-xs-12">
                <aside class="sidebar-content">
                    <div class="sidebar-title-account">
                        <h6>{{ $lang=='vn'?'TÀI KHOẢN':'MY ACCOUNT' }}</h6>
                    </div>
                    @if($lang=='vn')
                    <ul class="sidebar-tags-account">
                        <li><a href="{{ $url('tai-khoan/bang-dieu-khien') }}">quản lý tài khoản</a></li>
                        <li><a href="{{ $url('tai-khoan/thong-tin') }}">thông tin tài khoản</a></li>
                        <li><a href="{{ $url('tai-khoan/dia-chi') }}">địa chỉ</a></li>
                        <li><a href="{{ $url('tai-khoan/don-hang') }}" class="active">. thông tin đơn hàng</a></li>
                    </ul>
                    @else
                   	<ul class="sidebar-tags-account">
                        <li><a href="{{ $url('account/dashboard') }}">account dashboard</a></li>
                        <li><a href="{{ $url('account/information') }}">account information</a></li>
                        <li><a href="{{ $url('account/address-book') }}">address book</a></li>
                        <li><a href="{{ $url('account/orders') }}" class="active">. my orders</a></li>
                    </ul>
                    @endif
                </aside>
            </div>
            <!--left sidebar end -->

            <!--account right content start-->
            <div class="col-md-9 col-sm-8 col-xs-12">
                <div class="right-account-content">
                    <div class="intro-right-my-order-content">
                        <p style="text-transform: uppercase;">{{ trans('account.orders._order-no') }}: <span>OID.{{ $order->id }}</span></p>
                    </div>
                    <!--BLOCK CONTENT start-->
                    <div class="">
                        <div class="address-info-content">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="account-title-block">
                                        <h6>{{ trans('account.address-book._billing-address') }}</h6>
                                    </div>
                                    <div class="address-detail-content">
                                        @if(count($order->shippingData) > 0)
                                            <p style="text-transform: uppercase;">{{ strtoupper(Auth::user()->name) }}</p>
                                            <p style="text-transform: capitalize;">{{ trans('account.address-book._phone') }}: {{ $order->userData->phone }}</p>
                                            <p style="text-transform: capitalize;">{{ trans('account.address-book._address') }}: {{ $order->userData->address }}</p>
                                            <p style="text-transform: capitalize;">{{ trans('account.address-book._city') }}: {{ $order->userData->city }}</p>
                                            <p style="text-transform: capitalize;">{{ trans('account.address-book._postal-code') }}: {{ $order->userData->postal_code }}</p>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="account-title-block">
                                        <h6>{{ trans('account.orders._payment-method') }}</h6>
                                    </div>
                                    <div class="address-detail-content">
                                        <p>{{ strtoupper($order->payment_id) }}</p>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                    <!--BLOCK CONTENT end-->

                    <!--BLOCK CONTENT start-->
                    <div class="">
                        <div class="address-info-content">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="account-title-block">
                                        <h6>{{ trans('account.address-book._shipping-address') }}</h6>
                                    </div>
                                    <div class="address-detail-content">
                                        @if(count($order->shippingData) > 0)
                                            <p style="text-transform: uppercase;">{{ strtoupper($order->shippingData->name) }}</p>
                                            <p style="text-transform: capitalize;">{{ trans('account.address-book._phone') }}: {{ $order->shippingData->phone }}</p>
                                            <p style="text-transform: capitalize;">{{ trans('account.address-book._address') }}: {{ $order->shippingData->address }}</p>
                                            <p style="text-transform: capitalize;">{{ trans('account.address-book._city') }}: {{ $order->shippingData->city }}</p>
                                            <p style="text-transform: capitalize;">{{ trans('account.address-book._postal-code') }}: {{ $order->shippingData->postal_code }}</p>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="account-title-block">
                                        <h6>{{ trans('account.orders._shipping-method') }}</h6>
                                    </div>
                                    <div class="address-detail-content">
                                        <p style="text-transform: uppercase;">{{ trans('account.orders._standard') }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--BLOCK CONTENT end-->

                    <!--BLOCK CONTENT start-->
                    <div class="">
                        <div class="account-title-block">
                            <h6>{{ trans('account.orders._order-detail') }}</h6>
                        </div>
                        <!--TABLE order start-->
                        <div class="table-account-order">
                            @if($order->orderDetail)
                                <table class="table">
                                    <tr>
                                        <td class="text-center" style="text-transform: uppercase;">{{ trans('account.orders._image') }}</td>
                                        <td class="text-center" style="text-transform: uppercase;">{{ trans('account.orders._product') }}</td>
                                        <td class="text-center" style="text-transform: uppercase;">{{ trans('account.orders._price') }}</td>
                                        <td class="text-center" style="text-transform: uppercase;">{{ trans('account.orders._qty') }}</td>
                                        <td style="text-transform: uppercase;">{{ trans('account.orders._subtotal') }}</td>
                                    </tr>
                                    @foreach($order->orderDetail as $item)
                                    <tr>
                                        <td class="text-center">
                                            @if($item->productData->mediaData)
                                            <a class="pop">
                                                <img src="{{ $url('public'.Storage::url($item->productData->mediaData[0]->images)) }}" style="height: 80px;">
                                            </a>
                                            @endif
                                            <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-body">
                                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                            <img src="" class="imagepreview">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>

                                        <td class="text-center">
                                            <p class="table-product-name">{{ $item->product_name }}</p>
                                            {!! $item->description !!}
                                        </td>
                                        <td class="text-center">{{ number_format($item->unit_cost,0,',','.') }} VND</td>
                                        <td class="text-center">{{ $item->qty }}</td>
                                        <td>{{ number_format($item->price,0,',','.') }} VND</td>

                                    </tr>
                                    @endforeach
                                    <?php
                                    $subtotal = ($order->price-$setting->shipping_fee)/(1+$setting->VAT/100);
                                    $VAT = $subtotal*($setting->VAT/100);
                                    ?>
                                    <tr>
                                        <td colspan="4" class="text-right" style="text-transform: uppercase;">{{ trans('account.orders._subtotal') }}</td>
                                        <td>{{ number_format($subtotal,0,',','.') }} VND</td>
                                    </tr>

                                    <tr class="subtotal-table-section">
                                        <td colspan="4" class="text-right">VAT({{ $setting->VAT }}%)</td>
                                        <td>{{ number_format($VAT,0,',','.') }} VND</td>
                                    </tr>
                                    @if($setting->shipping_fee > 0)
                                    <tr class="subtotal-table-section">
                                        <td colspan="4" class="text-right"style="text-transform: uppercase;">{{ trans('account.orders._shipping') }}</td>
                                        <td>{{ number_format($setting->shipping_fee,0,',','.') }} VND</td>
                                    </tr>
                                    @endif
                                    <tr class="subtotal-table-section">
                                        <td class="subtotal-total-text text-right" colspan="4"style="text-transform: uppercase;">{{ trans('account.orders._total') }}</td>
                                        <td class="subtotal-total-text">{{ number_format($order->price,0,',','.') }} VND</td>

                                    </tr>
                                </table>
                            @endif
                        </div>
                        <!--TABLE ORDER end-->
                    </div>
                    <!--BLOCK CONTENT end-->
                </div>
            </div>
            <!-- account right content end -->
        </div>
    </div>
</div>
<!--account area end-->

@stop
