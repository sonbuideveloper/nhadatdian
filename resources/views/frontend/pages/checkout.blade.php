@extends('frontend.master')
@section('content')
<?php $url = env('HTTP_URL'); ?>
    <!-- breadcrumbs area start -->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="container-inner">
                        <ul>
                            <li class="home">
                                <a href="{{ $url('/') }}">{{ $lang=='vn'?'Trang chủ':'Home' }}</a>
                                <span><i class="fa fa-angle-right"></i></span>
                            </li>
                            <li class="category3"><span>{{ $lang=='vn'?'Thanh toán':'Checkout' }}</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumbs area end -->
    <!-- START MAIN CONTAINER -->
    <div class="main-container">
        <div class="product-cart">
            <div class="container">
                <div class="row">
                    <div class="checkout-content">
                        <div class="col-md-3 category-checkout">
                        <h5>{{ $lang=='vn'?'TIẾN TRÌNH KIỂM TRA':'YOUR CHECKOUT PROGRESS'}}</h5>
                        <ul>
                            <li><a class="link-hover">{{ $lang=='vn'?'Thông tin cá nhân':'Personal Info' }}</a></li>
                            <li><a class="link-hover">{{ $lang=='vn'?'Thông tin địa chỉ':'Address Info' }}</a></li>                                            
                            <li><a class="link-hover">{{ $lang=='vn'?'Phương thức thanh toán':'Payment Method' }}</a></li>
                            <li><a class="link-hover">{{ $lang=='vn'?'Xác nhận đơn hàng':'Confirm Order' }}</a></li>
                        </ul>
                    </div>
                        <div class="col-md-9 check-out-blok">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel checkout-accordion">
                                    <div class="panel-heading" role="tab" id="headingOne">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" aria-expanded="true" aria-controls="checkoutMethod">
                                                <span>1</span> {{ $lang=='vn'?'phương thức thanh toán':'Checkout method' }}
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="checkoutMethod" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                        <div class="content-info">
                                            @if(Session::has('flash_message'))                                                
                                                <div class="alert alert-{!! Session::get('flash_level') !!}">
                                                    {!! Session::get('flash_message') !!}
                                                </div>                                                
                                            @endif
                                            <div class="col-md-12">
                                                <form class="form-horizontal" method="post" action="{{ $lang=='vn'?$url('user/login-checkout'):$url('en/user/login-checkout') }}">
                                                    {!! csrf_field() !!}
                                                <div class="checkout-login">
                                                    <div class="checkTitle">
                                                        <h2 class="ct-design">{{ $lang=='vn'?'Đăng nhập':'Login' }}</h2>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="login-email" class="col-sm-2 control-label">* {{ $lang=='vn'?'Địa chỉ email':'Email Address' }}</label>
                                                        <div class="col-sm-10">
                                                            <input type="email" class="form-control" name="login-email" id="login-email" placeholder="Email">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="login-password" class="col-sm-2 control-label">* {{ $lang=='vn'?'Mật khẩu':'Password' }}</label>
                                                        <div class="col-sm-10">
                                                            <input type="password" class="form-control" name="login-password" id="login-password" placeholder="{{ $lang=='vn'?'Mật khẩu':'Password' }}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-offset-2 col-sm-10">
                                                            <div class="checkbox">
                                                                <label><input type="checkbox"> {{ $lang=='vn'?'Ghi nhớ':'Remember me' }}</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">                                                        
                                                        <div class="col-sm-offset-2 col-sm-10">
                                                            <button class="checkPageBtn button-login-page" type="submit" style="margin-bottom: 10px;">{{ $lang=='vn'?'Đăng nhập':'Login' }}</button>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">                                                        
                                                        <div class="col-sm-offset-2 col-sm-10">
                                                            <a href="{{ $url($lang=='vn'?'tai-khoan/dang-nhap':'user/login') }}" class="fgetpass">{{ $lang=='vn'?'Đăng ký tài khoản':'Register Account' }}</a>
                                                        </div>
                                                    </div>                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    
                </div>
                <!-- div.info-section -->
            </div>
            <!-- Checkout Container -->
            <div class="clearfix"></div>
        </div><!-- product-cart -->
    </div>
    <!-- END MAIN CONTAINER -->
    <div class="clearfix"></div>



@stop