@extends('frontend.master')
@section('content')
<?php $url = env('HTTP_URL'); ?>
<!--account area start-->
<div class="account-area">
    <div class="container">
        <div class="row">
            <!-- left sidebar start -->
            <div class="col-md-3 col-sm-4 col-xs-12">
                <aside class="sidebar-content">
                    <div class="sidebar-title-account">
                        <h6>{{ $lang=='vn'?'TÀI KHOẢN':'MY ACCOUNT' }}</h6>
                    </div>
                    @if($lang=='vn')
                    <ul class="sidebar-tags-account">
                        <li><a href="{{ $url('tai-khoan/bang-dieu-khien') }}">quản lý tài khoản</a></li>
                        <li><a href="{{ $url('tai-khoan/thong-tin') }}" class="active">. thông tin tài khoản</a></li>
                        <li><a href="{{ $url('tai-khoan/dia-chi') }}">địa chỉ</a></li>
                        <li><a href="{{ $url('tai-khoan/don-hang') }}">thông tin đơn hàng</a></li>
                    </ul>
                    @else
                   	<ul class="sidebar-tags-account">
                        <li><a href="{{ $url('account/dashboard') }}">account dashboard</a></li>
                        <li><a href="{{ $url('account/information') }}" class="active">. account information</a></li>
                        <li><a href="{{ $url('account/address-book') }}">address book</a></li>
                        <li><a href="{{ $url('account/orders') }}">my orders</a></li>
                    </ul>
                    @endif
                </aside>
            </div>
            <!--left sidebar end -->

            <!--account right content start-->
            <div class="col-md-9 col-sm-8 col-xs-12">
                <div class="right-account-content">
                    <form id="submitForm" class="form-horizontal" method="post" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <label class="control-label avatar-label col-sm-3" for="account_fname">{{ trans('account.information._avatar') }}:</label>
                            <div class="col-sm-9">
                                <div class="box-thumb-small">
                                    <div class="thumb-code-small" onclick="openUploadForm($('.favatar'),$('.imavatar>img'))">
                                        <a class="color-black  imavatar" href="#">
                                            <img class="avatar-holder" src="{{ Auth::user()->images?$url('public'.Storage::url(Auth::user()->images)):url('public/frontend/img/no-image.jpg') }}">
                                            <i class="fa fa-lg fa-plus add-img" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                    <input type="file" name="avatar" style="display:none" class="favatar">
                                </div>
                            </div>
                        </div>

                        <div class="account-info-input">
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="account_fname" style="text-transform: uppercase;">{{ trans('account.information._firstname') }}</label>
                                <div class="col-sm-9">
                                    <input type="text" id="account_fname" name="firstname" value="{{ Auth::user()->firstname }}" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-3" for="account_lname" style="text-transform: uppercase;">{{ trans('account.information._lastname') }}</label>
                                <div class="col-sm-9">
                                    <input type="text" id="account_lname" name="lastname" value="{{ Auth::user()->lastname }}" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-3" for="account_email">{{ trans('account.information._email') }}</label>
                                <div class="col-sm-9">
                                    <input type="email" class="form-control" name="email" id="account_email" value="{{ Auth::user()->email }}">
                                </div>
                            </div>
                            <div class="form-group">

                                <label class="control-label col-sm-3"></label>
                                <div class="col-sm-9">
                                    <a id="change-password">{{ trans('account.information._change_password') }}</a>
                                </div>
                            </div>

                            <div class="password-block">
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="account_password">{{ trans('account.information._password') }}</label>
                                    <div class="col-sm-9">
                                        <input type="password" id="account_password" name="password" class="form-control" placeholder="{{ trans('account.information._old_pass') }}" style="text-align: center">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="account_newpassword"></label>
                                    <div class="col-sm-9">
                                        <input type="password" id="account_newpassword" name="new_password" class="form-control" placeholder="{{ trans('account.information._new_pass') }}" style="text-align: center">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-3" for="account_newpassword"></label>
                                <div class="col-sm-9">
                                    <button class="submit-account-info" type="submit">{{ trans('account.information._save') }}</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- account right content end -->  
            </div>
        </div>
    </div>
</div>
<!--account area end-->

@stop