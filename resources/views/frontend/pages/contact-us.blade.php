@extends('frontend.master')
@section('content')
    <!-- Contact -->
    <section class="site-content site-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-6 site-block">
                    <div class="site-block">
                        <h3 class="h2 site-heading"><strong>Giới thiệu</strong></h3>
                        <p class="remove-margin">
                            {!! stripslashes($contact->content) !!}
                        </p>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 site-block">
                    <h3 class="h2 site-heading"><strong>Liên hệ</strong></h3>
                    <form action="" method="post" id="form-contact">
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <label for="contact-name">Tên</label>
                            <input required type="text" id="contact-name" name="contact-name" class="form-control input-lg" placeholder="Tên của bạn..">
                        </div>
                        <div class="form-group">
                            <label for="contact-email">Email</label>
                            <input required type="email" id="contact-email" name="contact-email" class="form-control input-lg" placeholder="Email của bạn..">
                        </div>
                        <div class="form-group">
                            <label for="contact-message">Thông điệp</label>
                            <textarea id="contact-message" name="contact-message" rows="10" class="form-control input-lg" placeholder="Nội dung.."></textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-lg btn-primary">Gửi</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        {{--@if($setting->adds_script)--}}
            {{--<div class="container">--}}
                {{--<div class="row">--}}
                    {{--<div style="margin-bottom: 30px;">--}}
                        {{--{!! $setting->adds_script !!}--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--@endif--}}
    </section>
    <!-- END Contact -->

@stop