@extends('frontend.master')
@section('content')
<?php $url = env('HTTP_URL'); ?>
<!--account area start-->
<div class="account-address-area">
    <div class="container">
        <div class="row">
            <!-- left sidebar start -->
            <div class="col-md-3 col-sm-4 col-xs-12">
                <aside class="sidebar-content">
                    <div class="sidebar-title-account">
                        <h6>{{ $lang=='vn'?'TÀI KHOẢN':'MY ACCOUNT' }}</h6>
                    </div>
                    @if($lang=='vn')
                    <ul class="sidebar-tags-account">
                        <li><a href="{{ $url('tai-khoan/bang-dieu-khien') }}">quản lý tài khoản</a></li>
                        <li><a href="{{ $url('tai-khoan/thong-tin') }}">thông tin tài khoản</a></li>
                        <li><a href="{{ $url('tai-khoan/dia-chi') }}" class="active">. địa chỉ</a></li>
                        <li><a href="{{ $url('tai-khoan/don-hang') }}">thông tin đơn hàng</a></li>
                    </ul>
                    @else
                   	<ul class="sidebar-tags-account">
                        <li><a href="{{ $url('account/dashboard') }}">account dashboard</a></li>
                        <li><a href="{{ $url('account/information') }}">account information</a></li>
                        <li><a href="{{ $url('account/address-book') }}" class="active">. address book</a></li>
                        <li><a href="{{ $url('account/orders') }}">my orders</a></li>
                    </ul>
                    @endif
                </aside>
            </div>
            <!--left sidebar end -->

            <!--account right content start-->
                <div class="col-md-9 col-sm-8 col-xs-12">
                    <div class="right-account-content">
                        <div class="account-title-block">
                            <h6>{{ trans('account.address-book._address-book') }}</h6>
                        </div>

                        <div class="address-info-content">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="address-detail-content" id="billing-popup">
                                        <div class="address-edit-block">
                                            <h6>{{ trans('account.address-book._billing-address') }} |</h6>
                                            <a id="edit-billing-address"><span style="text-transform: uppercase;">{{ trans('account.address-book._edit') }}</span></a>
                                        </div>

                                        @if($billing_data)
                                            <p style="text-transform: uppercase;">{{ Auth::user()->name }}</p>
                                            <p style="text-transform: capitalize;">{{ trans('account.address-book._phone') }}: {{ $billing_data->phone }}</p>
                                            <p style="text-transform: capitalize;">{{ trans('account.address-book._address') }}: {{ $billing_data->address }}</p>
                                            <p style="text-transform: capitalize;">{{ trans('account.address-book._city') }}: {{ $billing_data->city }}</p>
                                            <p style="text-transform: capitalize;">{{ trans('account.address-book._postal-code') }}: {{ $billing_data->postal_code }}</p>                                            
                                        @endif  
                                    </div>

                                    <div class="billing-frame" style="display:none;">
                                        <img src="{{ $url('frontend/img/close-button.png') }}" id="close-edit-billing">
                                        <div class="address-edit-block">
                                            <h6>{{ trans('account.address-book._billing-address') }}</h6>                                            
                                        </div>
                                        <form action="{{ trans('account.url._address') }}/{{ trans('account.url._edit') }}" class="form-horizontal" method="post">     
                                            {!! csrf_field() !!}
                                            <input type="hidden" name="slug" value="billing">                                       
                                            <div class="form-group">
                                                <label for="billing_phone" style="text-transform: uppercase;">{{ trans('account.address-book._phone') }}:</label><input name="phone" id="billing_phone" type="text" value="{{ $billing_data->phone }}">
                                            </div>
                                            <div class="form-group">
                                                <label for="billing_address" style="text-transform: uppercase;">{{ trans('account.address-book._address') }}:</label><input name="address" id="billing_address" type="text" value="{{ $billing_data->address }}">
                                            </div>
                                            <div class="form-group">
                                                <label for="billing_city" style="text-transform: uppercase;">{{ trans('account.address-book._city') }}:</label><input name="city" id="billing_city" type="text" value="{{ $billing_data->city }}">
                                            </div>
                                            <div class="form-group">
                                                <label for="billing_country" style="text-transform: uppercase;">{{ trans('account.address-book._postal-code') }}:</label><input name="postal_code" id="billing_country" type="text" value="{{ $billing_data->postal_code }}">
                                            </div>                                    
                                            <button type="submit" class="edit-shipping-button" style="position:absolute; bottom:-60px">{{ trans('account.information._save') }}</button>
                                        </form>
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="address-detail-content" id="shipping-popup">

                                        <div class="address-edit-block">
                                            <h6>{{ trans('account.address-book._shipping-address') }} |</h6>
                                            <a id="edit-shipping-address"><span style="text-transform: uppercase;">{{ trans('account.address-book._edit') }}</span></a>
                                        </div>

                                        @if($shipping_data)
                                            <p style="text-transform: uppercase;">{{ $shipping_data->name }}</p>
                                            <p style="text-transform: capitalize;">{{ trans('account.address-book._phone') }}: {{ $shipping_data->phone }}</p>
                                            <p style="text-transform: capitalize;">{{ trans('account.address-book._address') }}: {{ $shipping_data->address }}</p>
                                            <p style="text-transform: capitalize;">{{ trans('account.address-book._city') }}: {{ $shipping_data->city }}</p>
                                            <p style="text-transform: capitalize;">{{ trans('account.address-book._postal-code') }}: {{ $shipping_data->postal_code }}</p>  
                                        @endif

                                    </div>

                                    <div class="shipping-frame" style="display:none">
                                        <img src="{{ $url('frontend/img/close-button.png') }}" id="close-edit-shipping">

                                        <div class="address-edit-block">
                                            <h6>{{ trans('account.address-book._shipping-address') }}</h6>                                           
                                        </div>
                                        <form action="{{ trans('account.url._address') }}/{{ trans('account.url._edit') }}" class="form-horizontal" method="post">
                                            {!! csrf_field() !!}
                                            <input type="hidden" name="slug" value="shipping">
                                            <div class="form-group">
                                                <label for="shipping_name" style="text-transform: uppercase;">{{ trans('account.address-book._name') }}:</label><input name="name" id="shipping_name" type="text" value="{{ $shipping_data->name }}">
                                            </div>
                                            <div class="form-group">
                                                <label for="shipping_phone" style="text-transform: uppercase;">{{ trans('account.address-book._phone') }}:</label><input name="phone" id="shipping_phone" type="text" value="{{ $shipping_data->phone }}">
                                            </div>
                                            <div class="form-group">
                                                <label for="shipping_address" style="text-transform: uppercase;">{{ trans('account.address-book._address') }}:</label><input name="address" id="shipping_address" type="text" value="{{ $shipping_data->address }}">
                                            </div>
                                            <div class="form-group">
                                                <label for="shipping_city" style="text-transform: uppercase;">{{ trans('account.address-book._city') }}:</label><input name="city" id="shipping_city" type="text" value="{{ $shipping_data->city }}">
                                            </div>
                                            <div class="form-group">
                                                <label for="shipping_country" style="text-transform: uppercase;">{{ trans('account.address-book._postal-code') }}:</label><input name="postal_code" id="shipping_country" type="text" value="{{ $shipping_data->postal_code }}">
                                            </div>
                                            <button type="submit" class="edit-shipping-button" style="position:absolute; bottom:-60px">{{ trans('account.information._save') }}</button>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- account right content end -->


                    </div>

                </div>

        </div>
    </div>
</div>
<!--account area end-->

@stop