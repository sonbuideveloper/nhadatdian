@extends('frontend.master')
@section('content')

    <section class="site-content site-section">
        <div class="container">
            <div class="row">
                <!-- Posts -->
                <div class="col-sm-8 col-md-9">
                    <h1 class="title">KẾT QUẢ TÌM KIẾM</h1>
                    @if(count($articles) > 0)
                        @foreach($articles as $item)
                            <div class="site-block">
                                <div class="row">
                                    <div class="col-md-4">
                                        <p>
                                            <a href="{{ url('/bai-viet/'.$item->slug) }}">
                                                <img src="{{ url(Storage::url($item->images)) }}" alt="image" class="img-responsive">
                                            </a>
                                        </p>
                                    </div>
                                    <div class="col-md-8">
                                        <a href="{{ url('/bai-viet/'.$item->slug) }}">
                                            <h3 class="site-heading">
                                                <strong>{{ $item->name }}</strong>
                                            </h3>
                                        </a>
                                        {!! stripslashes($item->description) !!}
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <p class="pull-right">
                                        <a href="{{ url('/bai-viet/'.$item->slug) }}" class="label label-primary">Xem chi tiết...</a>
                                    </p>
                                </div>
                            </div>
                        @endforeach


                        {{--@if($setting->adds_script)--}}
                            {{--<div style="margin-bottom: 30px;">--}}
                                {{--{!! $setting->adds_script !!}--}}
                            {{--</div>--}}
                        {{--@endif--}}
                    @else
                        <h2>Không có kết quả này. Vui lòng thử lại với từ khóa khác.</h2>
                    @endif
                </div>
                <!-- END Posts -->

                <!-- Sidebar -->
                @include('frontend.block.sidebar')
                <!-- END Sidebar -->
            </div>
        </div>
    </section>

@stop