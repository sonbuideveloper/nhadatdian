@extends('frontend.master')
@section('content')
<?php $url = env('HTTP_URL'); ?>
<!--account area start-->
<div class="account-address-area">
    <div class="container">
        <div class="row">
            <!-- left sidebar start -->
            <div class="col-md-3 col-sm-4 col-xs-12">
                <aside class="sidebar-content">
                    <div class="sidebar-title-account">
                        <h6>{{ $lang=='vn'?'TÀI KHOẢN':'MY ACCOUNT' }}</h6>
                    </div>
                    @if($lang=='vn')
                    <ul class="sidebar-tags-account">
                        <li><a href="{{ $url('tai-khoan/bang-dieu-khien') }}">quản lý tài khoản</a></li>
                        <li><a href="{{ $url('tai-khoan/thong-tin') }}">thông tin tài khoản</a></li>
                        <li><a href="{{ $url('tai-khoan/dia-chi') }}">địa chỉ</a></li>
                        <li><a href="{{ $url('tai-khoan/don-hang') }}" class="active">. thông tin đơn hàng</a></li>
                    </ul>
                    @else
                   	<ul class="sidebar-tags-account">
                        <li><a href="{{ $url('account/dashboard') }}">account dashboard</a></li>
                        <li><a href="{{ $url('account/information') }}">account information</a></li>
                        <li><a href="{{ $url('account/address-book') }}">address book</a></li>
                        <li><a href="{{ $url('account/orders') }}" class="active">. my orders</a></li>
                    </ul>
                    @endif
                </aside>
            </div>
            <!--left sidebar end -->

            <!--account right content start-->
            <div class="col-md-9 col-sm-8 col-xs-12">
                <div class="right-account-content-order">
                    <!--BLOCK CONTENT start-->
                    <div class="account-block-content">
                        <!--TABLE order start-->
                        <div class="table-account-order">
                            @if(count($orders) > 0)
                            <table class="table">
                                <tr>
                                    <td style="text-transform: uppercase;">{{ trans('account.orders._order-no') }}.</td>
                                    <td style="text-transform: uppercase;">{{ trans('account.orders._shipped') }}</td>
                                    <td style="text-transform: uppercase;">{{ trans('account.orders._total') }}</td>
                                    <td style="text-transform: uppercase;">{{ trans('account.orders._status') }}</td>
                                    <td style="text-transform: uppercase;">{{ trans('account.orders._date') }}</td>
                                    <td></td>
                                </tr>
                                @foreach($orders as $item)
                                    <tr>
                                        <td>OID.{{ $item->id }}</td>
                                            <td>{{ $item->shippingData->name }}</td>
                                            <td>{{ number_format($item->price,0,',','.') }} VND</td>                                            
                                            <?php 
                                                switch ($item->publish) {
                                                    case 1:
                                                        echo '<td><span class="text-primary" style="text-transform: capitalize;">'.trans("account.orders._processing").'</span></td>';
                                                        break;
                                                    case 2:
                                                        echo '<td><span class="text-muted" style="text-transform: capitalize;">'.trans("account.orders._shipping").'</span></td>';                                                        
                                                        break;
                                                    case 3:
                                                        echo '<td><span class="text-success" style="text-transform: capitalize;">'.trans("account.orders._completed").'</span></td>';                                                        
                                                        break;
                                                    case 4:
                                                        echo '<td><span class="text-danger" style="text-transform: capitalize;">'.trans("account.orders._canceled").'</span></td>';                                                       
                                                        break;
                                                    default: 
                                                        echo '<td></td>';                                                       
                                                        break;
                                                }
                                            ?>                                            
                                            <td>{{ convertDateTime($item->created_at) }}</td>
                                            <td class="link-to-view"><a href="{{ $url(trans('account.url._account').'/'.trans('account.url._order')).'/'.$item->id }}">{{ trans('account.orders._view') }}</a></td>
                                    </tr>
                                @endforeach
                            </table>
                            @else
                            <h3 class="text-center">{{ trans('account.orders._no-order') }}</h3>
                            @endif
                        </div>
                        <!--TABLE ORDER end-->

                    </div>
                    <!--BLOCK CONTENT end-->


                </div>
            </div>            
             <!-- account right content end -->
        </div>
    </div>
</div>
<!--account area end-->

@stop