@extends('frontend.master')
@section('content')
<?php $url = env('HTTP_URL'); ?>
    <!-- breadcrumbs area start -->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="container-inner">
                        <ul>
                            <li class="home">
                                <a href="{{ $url('/') }}">{{ $lang=='vn'?'Trang chủ':'Home' }}</a>
                                <span><i class="fa fa-angle-right"></i></span>
                            </li>
                            <li class="category3"><span>{{ $lang=='vn'?'Thông tin địa chỉ':'Address Info' }}</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumbs area end -->
    <!-- START MAIN CONTAINER -->
    <div class="main-container">
        <div class="product-cart">
            <div class="container">
                <div class="row">
                    <div class="checkout-content">
                        <div class="col-md-3 category-checkout">
                        <ul>
                            <li><a class="link-hover">{{ $lang=='vn'?'Kiểm tra':'Checkout method' }}</a></li>
                            <li><a class="link-hover">{{ $lang=='vn'?'Thông tin cá nhân':'Personal Info' }}</a></li>
                        </ul>
                        <h5><h5>{{ $lang=='vn'?'THÔNG TIN ĐỊA CHỈ':'YOUR BILLING PROGRESS'}}</h5></h5>
                        <ul>                                                                      
                            <li><a class="link-hover">{{ $lang=='vn'?'Phương thức thanh toán':'Payment Method' }}</a></li>
                            <li><a class="link-hover">{{ $lang=='vn'?'Xác nhận đơn hàng':'Confirm Order' }}</a></li>
                        </ul>
                    </div>
                        <div class="col-md-9 check-out-blok">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel checkout-accordion">
                                    <div class="panel-heading" role="tab" id="headingTwo">
                                        <h4 class="panel-title">
                                            <a class="" data-toggle="collapse" data-parent="#accordion" href="" aria-expanded="true" aria-controls="billingInformation">
                                                <span>3</span> {{ $lang=='vn'?'Thông tin địa chỉ':'Address Info' }}
                                            </a>
                                        </h4>
                                    </div>
                                    <div aria-labelledby="headingTwo">
                                        <div class="content-info">
                                            <form method="post" action="billing">
                                                {!! csrf_field() !!}  
                                                <input type="hidden" name="txtUserID" value="{{ Auth::user()->id }}">                                          
                                                <div class="col-sm-12">
                                                    @if (count($errors) > 0)
                                                        <div class="alert alert-danger">
                                                            <ul>
                                                                @foreach ($errors->all() as $error)
                                                                    <li>{{ $error }}</li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    @endif

                                                    <div class="col-md-6">
                                                        <h4 class="page-header">{{ $lang=='vn'?'Địa chỉ thanh toán':'Billing address' }}</h4>
                                                        <div class="form-group">
                                                            <label for="billing-name">{{ $lang=='vn'?'Họ và tên':'Name' }}</label>
                                                            <input type="text" id="billing-name" class="form-control" disabled="disabled" value="{{ Auth::user()->name }}" required="required">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="billing-address">{{ $lang=='vn'?'Địa chỉ':'Address' }}</label>
                                                            <input type="text" id="billing-address" name="billing-address" class="form-control" value="{{ $billing_data ? $billing_data['address'] : old('billing-address') }}" required="required">
                                                        </div>                                                    
                                                        <div class="form-group">
                                                            <label for="billing-postal">{{ $lang=='vn'?'Mã bưu chính':'Postal code' }}</label>
                                                            <input type="text" id="billing-postal" name="billing-postal" class="form-control" value="{{ $billing_data ? $billing_data['postal_code'] : old('billing-postal') }}" required="required">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="billing-phone">{{ $lang=='vn'?'Số điện thoại':'Phone' }}</label>
                                                            <input type="text" id="billing-phone" name="billing-phone" class="form-control" value="{{ $billing_data ? $billing_data['phone'] : old('billing-phone') }}" required="required">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="billing_city">{{ $lang=='vn'?'Thành phố':'City' }}</label>
                                                            <input type="text" id="billing_city" name="billing-city" class="form-control" value="{{ $billing_data ? $billing_data['city'] : old('billing-city') }}" required="required">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <h4 class="page-header">{{ $lang=='vn'?'Địa chỉ giao hàng':'Shipping Address' }}</h4>
                                                        <div class="form-group">
                                                            <label for="shipping-name">{{ $lang=='vn'?'Họ và tên':'Name' }}</label>
                                                            <input type="text" id="shipping-name" name="shipping-name" class="form-control" value="{{ $shipping_data ? $shipping_data['name'] : old('shipping-name') }}" required="required">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="shipping-address">{{ $lang=='vn'?'Địa chỉ':'Address' }}</label>
                                                            <input type="text" id="shipping-address" name="shipping-address" class="form-control" value="{{ $shipping_data ? $shipping_data['address'] : old('shipping-address') }}" required="required">
                                                        </div>                                                    
                                                        <div class="form-group">
                                                            <label for="shipping-postal">{{ $lang=='vn'?'Mã bưu chính':'Postal code' }}</label>
                                                            <input type="text" id="shipping-postal" name="shipping-postal" class="form-control" value="{{ $shipping_data ? $shipping_data['postal_code'] : old('shipping-postal') }}" required="required">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="shipping-phone">{{ $lang=='vn'?'Số điện thoại':'Phone' }}</label>
                                                            <input type="text" id="shipping-phone" name="shipping-phone" class="form-control" value="{{ $shipping_data ? $shipping_data['phone'] : old('shipping-phone') }}" required="required">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="shipping-city">{{ $lang=='vn'?'Thành phố':'City' }}</label>
                                                            <input type="text" id="shipping-city" name="shipping-city" class="form-control" value="{{ $shipping_data ? $shipping_data['city'] : old('shipping-city') }}" required="required">
                                                        </div>
                                                    </div>                                                    
                                                </div>                                                
                                                <div class="form-group">                                                        
                                                    <div class="col-sm-12 text-right">
                                                    	@if($lang=='vn')
	                                                        <a href="{{ $url('thong-tin-ca-nhan') }}" class="button-login-page-prev" id="pre">Quay lại</a>
	                                                        <input type="submit" class="button-login-page" id="next" value="Tiếp theo">
                                                        @else
	                                                        <a href="{{ $url('personal-info') }}" class="button-login-page-prev" id="pre">Previous</a>
	                                                        <input type="submit" class="button-login-page" id="next" value="Next">
                                                        @endif
                                                    </div>
                                                </div>
                                            </form>                                
                                        </div>
                                    </div>
                                </div>                            
                            </div>
                        </div>
                    </div>
                    
                </div>
                <!-- div.info-section -->
            </div>
            <!-- Checkout Container -->
            <div class="clearfix"></div>
        </div><!-- product-cart -->
    </div>
    <!-- END MAIN CONTAINER -->
    <div class="clearfix"></div>



@stop