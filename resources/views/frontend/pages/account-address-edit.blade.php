@extends('frontend.master')
@section('content')
<?php $url = env('HTTP_URL'); ?>
<!--account area start-->
<div class="account-address-area">
    <div class="container">
        <div class="row">
            <!-- left sidebar start -->
            <div class="col-md-3 col-sm-4 col-xs-12">
                <aside class="sidebar-content">
                    <div class="sidebar-title-account">
                        <h6>{{ $lang=='vn'?'TÀI KHOẢN':'MY ACCOUNT' }}</h6>
                    </div>
                    @if($lang=='vn')
                    <ul class="sidebar-tags-account">
                        <li><a href="{{ $url('tai-khoan/bang-dieu-khien') }}">quản lý tài khoản</a></li>
                        <li><a href="{{ $url('tai-khoan/thong-tin') }}">thông tin tài khoản</a></li>
                        <li><a href="{{ $url('tai-khoan/dia-chi') }}" class="active">. địa chỉ</a></li>
                        <li><a href="{{ $url('tai-khoan/don-hang') }}">thông tin đơn hàng</a></li>
                    </ul>
                    @else
                   	<ul class="sidebar-tags-account">
                        <li><a href="{{ $url('account/dashboard') }}">account dashboard</a></li>
                        <li><a href="{{ $url('account/information') }}">account information</a></li>
                        <li><a href="{{ $url('account/address-book') }}" class="active">. address book</a></li>
                        <li><a href="{{ $url('account/orders') }}">my orders</a></li>
                    </ul>
                    @endif
                </aside>
            </div>
            <!--left sidebar end -->

            <!--account right content start-->
                <div class="col-md-9 col-sm-8 col-xs-12">
                    <div class="right-account-content">
                        <div class="account-title-block">
                        	@if($lang=='vn')
                            <h6>
                            	ĐỊA CHỈ {{ $slug=='thanh-toan'?'THANH TOÁN':'GIAO HÀNG' }}
                            </h6>
                            @else
                            <h6>{{ strtoupper($slug) }} ADDRESS</h6>
                            @endif
                        </div>

                        <form action="{{ trans('account.url._edit') }}" class="form-horizontal" method="post">
                            {!! csrf_field() !!}
                            <div class="account-info-input">
                                <input type="hidden" name="slug" value="{{ $slug }}">
                                @if($slug == 'shipping' || $slug == 'giao-hang')
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="account_address_name" style="text-transform: uppercase;">{{ trans('account.address-book._name') }}</label>
                                    <div class="col-sm-9">
                                        <input type="text" id="account_address_name" name="name" value="{{ isset($data->name) ? $data->name : old('name')}}" class="form-control">
                                    </div>
                                </div>
                                @endif
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="account_address_phone" style="text-transform: uppercase;">{{ trans('account.address-book._phone') }}</label>
                                    <div class="col-sm-9">
                                        <input type="text" id="account_address_phone" name="phone" value="{{ isset($data->phone) ? $data->phone : old('phone')}}" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="account_address" style="text-transform: uppercase;">{{ trans('account.address-book._address') }}</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="account_address" name="address" value="{{ isset($data->address) ? $data->address : old('address')}}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="account_address_city" style="text-transform: uppercase;" >{{ trans('account.address-book._city') }}</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="account_address_city" name="city" value="{{ isset($data->city) ? $data->city : old('city')}}">
                                    </div>
                                </div>
                                <div class="form-group">

                                    <label class="control-label col-sm-3" for="account_address_country" style="text-transform: uppercase;">{{ trans('account.address-book._postal-code') }}</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="account_address_country" name="postal_code" value="{{ isset($data->postal_code) ? $data->postal_code : old('postal_code')}}">
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="control-label col-sm-3" for=""></label>
                                    <div class="col-sm-9">
                                        <button class="submit-account-info" type="submit">{{ trans('account.information._save') }}</button> 
                                    </div>
                                </div>

                            </div>



                        </form>

                    </div>
                </div>
                <!-- account right content end -->
        </div>
    </div>
</div>
<!--account area end-->

@stop