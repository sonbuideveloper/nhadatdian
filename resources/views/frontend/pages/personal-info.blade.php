@extends('frontend.master')
@section('content')
<?php $url = env('HTTP_URL'); ?>
    <!-- breadcrumbs area start -->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="container-inner">
                        <ul>
                            <li class="home">
                                <a href="{{ $url('/') }}">{{ $lang=='vn'?'Trang chủ':'Home' }}</a>
                                <span><i class="fa fa-angle-right"></i></span>
                            </li>
                            <li class="category3"><span>{{ $lang=='vn'?'Thông tin cá nhân':'Personal Info' }}</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumbs area end -->
    <!-- START MAIN CONTAINER -->
    <div class="main-container">
        <div class="product-cart" style="min-height:380px">
            <div class="container">
                <div class="row">
                    <div class="checkout-content">
                         <div class="col-md-3 category-checkout">
                        <ul>                            
                            <li><a class="link-hover">{{ $lang=='vn'?'Kiểm tra':'Checkout method' }}</a></li>
                        </ul>
                        <h5>{{ $lang=='vn'?'THÔNG TIN CÁ NHÂN':'YOUR PERSONAL PROGRESS'}}</h5>
                        <ul>
                            <li><a class="link-hover">{{ $lang=='vn'?'Thông tin địa chỉ':'Address Info' }}</a></li>                                            
                            <li><a class="link-hover">{{ $lang=='vn'?'Phương thức thanh toán':'Payment Method' }}</a></li>
                            <li><a class="link-hover">{{ $lang=='vn'?'Xác nhận đơn hàng':'Confirm Order' }}</a></li>
                        </ul>
                    </div>
                        <div class="col-md-9 check-out-blok">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel checkout-accordion">
                                    <div class="panel-heading" role="tab" id="headingTwo">
                                        <h4 class="panel-title">
                                            <a class="" data-toggle="collapse" data-parent="#accordion" href="" aria-expanded="true" aria-controls="billingInformation">
                                                <span>2</span> {{ $lang=='vn'?'Thông tin cá nhân':'Personal Info' }}
                                            </a>
                                        </h4>
                                    </div>
                                    <div aria-labelledby="headingTwo">
                                        <div class="content-info">
                                            <form method="post" class="form-horizontal" action="personal-info">
                                                {!! csrf_field() !!}  
                                                <input type="hidden" name="txtUserID" value="{{ Auth::user()->id }}">                                          
                                                <div class="col-sm-12">
                                                    @if (count($errors) > 0)
                                                        <div class="alert alert-danger">
                                                            <ul>
                                                                @foreach ($errors->all() as $error)
                                                                    <li>{{ $error }}</li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    @endif

                                                    <div class="form-group">
                                                        <label for="email" class="col-sm-2 control-label">Email</label>
                                                        <div class="col-sm-10">
                                                            <input type="email" class="form-control" disabled="disabled" id="email" value="{{ Auth::user()->email }}">
                                                        </div>
                                                    </div> 
                                                    <div class="form-group">
                                                        <label for="firstname" class="col-sm-2 control-label" style="text-transform:capitalize;">{{ trans('account.information._firstname') }}</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control" id="firstname" name="firstname" value="{{ Auth::user()->firstname ? Auth::user()->firstname : old('firstname') }}">
                                                        </div>
                                                    </div>                 
                                                    <div class="form-group">
                                                        <label for="lastname" class="col-sm-2 control-label" style="text-transform:capitalize;">{{ trans('account.information._lastname') }}</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control" id="lastname" name="lastname" value="{{ Auth::user()->lastname ? Auth::user()->lastname : old('lastname') }}">
                                                        </div>
                                                    </div> 
                                                    <div class="form-group">   
                                                        <div class="col-sm-offset-2 col-sm-10">          
                                                        	@if($lang=='vn')                       
                                                            	<a href="{{ $url('gio-hang') }}" class="button-login-page-prev" id="pre">Quay lại</a>
                                                            	<input type="submit" class="button-login-page" id="next" value="Tiếp theo">
                                                            @else
                                                            	<a href="{{ $url('cart') }}" class="button-login-page-prev" id="pre">Previous</a>
                                                            	<input type="submit" class="button-login-page" id="next" value="Next">
                                                            @endif                                                           
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>                                
                                        </div>
                                    </div>
                                </div>                            
                            </div>
                        </div>
                    </div>
                   
                </div>
                <!-- div.info-section -->
            </div>
            <!-- Checkout Container -->
            <div class="clearfix"></div>
        </div><!-- product-cart -->
    </div>
    <!-- END MAIN CONTAINER -->
    <div class="clearfix"></div>



@stop