<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">
        <title class="meta-title">{{ $meta['title'] }}</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="content-language" content="vn" />
        <meta name="robots" content="noodp,noydir,index,follow " />
        <meta class="meta-description" name="description" content="{!! $meta['description'] !!}">
        <meta class="meta-keywords" name="keywords" content="{!! $meta['keywords'] !!}">
        <meta class="meta-title" name="title" content="{{ $meta['title'] }}">
        <meta class="meta-title" property="og:title" content="{{ $meta['title'] }}" />
        <meta class="meta-images" property="og:image" content="{{ $meta['images'] }}" />
        <meta property="og:site_name" content="{{ $meta['title'] }}" />
        <meta class="meta-url" property="og:url" content="{{ url()->current() }}" />
        <meta class="meta-keywords" property="og:keywords" content="{!! $meta['keywords'] !!}" />
        <meta class="meta-description" property="og:description" content="{!! $meta['description'] !!}" />
        <meta name="twitter:card" content="summary_large_image" />
        <meta class="meta-description" name="twitter:description" content="{!! $meta['description'] !!}" />
        <meta class="meta-title" name="twitter:title" content="{{ $meta['title'] }}" />
        <meta name="twitter:site" content="{{ url()->current() }}" />
        <meta class="meta-images" name="twitter:image" content="{{ $meta['images'] }}" />
        <link rel="author" href="https://plus.google.com/sonbui" />
        <link rel="canonical" href="{{ url('/') }}" />
        <meta property="og:type" content="article">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="{{ url('frontend/img/favicon.png') }}">
        <link rel="apple-touch-icon" href="{{ url('frontend/img/icon57.png') }}" sizes="57x57">
        <link rel="apple-touch-icon" href="{{ url('frontend/img/icon72.png') }}" sizes="72x72">
        <link rel="apple-touch-icon" href="{{ url('frontend/img/icon76.png') }}" sizes="76x76">
        <link rel="apple-touch-icon" href="{{ url('frontend/img/icon114.png') }}" sizes="114x114">
        <link rel="apple-touch-icon" href="{{ url('frontend/img/icon120.png') }}" sizes="120x120">
        <link rel="apple-touch-icon" href="{{ url('frontend/img/icon144.png') }}" sizes="144x144">
        <link rel="apple-touch-icon" href="{{ url('frontend/img/icon152.png') }}" sizes="152x152">
        <link rel="apple-touch-icon" href="{{ url('frontend/img/icon180.png') }}" sizes="180x180">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="{{ url('frontend/css/bootstrap.min.css') }}">

        <!-- Related styles of various icon packs and plugins -->
        <link rel="stylesheet" href="{{ url('frontend/css/plugins.css') }}">

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        <link rel="stylesheet" href="{{ url('frontend/css/main.css') }}">

        <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
        <link rel="stylesheet" href="{{ url('frontend/css/themes.css') }}">
        <!-- END Stylesheets -->

        <!-- Modernizr (browser feature detection library) & Respond.js (enables responsive CSS code on browsers that don't support it, eg IE8) -->
        <script src="{{ url('frontend/js/vendor/modernizr-respond.min.js') }}"></script>

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-109838018-4"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-109838018-4');
        </script>

        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({
                google_ad_client: "ca-pub-5537233573134398",
                enable_page_level_ads: true
            });
        </script>


    </head>
<style>
    htmml{
        overflow-y: hidden;
    }
    h1.title{
        font-weight: bold;
        margin-top: 0;
        margin-bottom: 30px;
        padding-bottom: 10px;
        border-bottom: 1px solid #eeeeee;
        font-size: 25px;
    }
    .site-block{
        border-bottom: 1px solid #eeeeee;
    }
    .site-footer.site-section{
        padding-bottom: 0;
    }
    .footer-heading{
        margin-top: 10px;
    }
    @media (max-width: 991px){
        .footer-heading{
            font-size: 13px;
            text-align: center;
        }
        .footer-nav.footer-nav-social.list-inline{
            text-align: center;
        }
    }
</style>
    <body>
        <!-- Page Container -->
        <!-- In the PHP version you can set the following options from inc/config file -->
        <!-- 'boxed' class for a boxed layout -->
        <div id="page-container">
            @include('frontend.block.error')
            <!-- Site Header -->
            @include('frontend.block.header')
            <!-- END Site Header -->

            <!-- Intro -->
            <section class="site-section site-section-light site-section-top themed-background-dark" style="background: url({{ Storage::url($banner->mediaData[0]->images) }}); background-size: cover; background-position: center; height: 50vh;">
                <div class="container">
                    <h1 class="animation-slideDown"><strong></strong></h1>
                    <h2 class="h3 animation-slideUp"></h2>
                </div>
            </section>
            <!-- END Intro -->

            <!-- Content -->
            @yield('content')
            <!-- END Content -->

            <!-- Footer -->
            @include('frontend.block.footer')
            <!-- END Footer -->
        </div>
        <!-- END Page Container -->

        <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
        <a href="#" id="to-top"><i class="fa fa-angle-up"></i></a>

        <!-- jQuery, Bootstrap.js, jQuery plugins and Custom JS code -->
        <script src="{{ url('frontend/js/vendor/jquery-1.12.0.min.js') }}"></script>
        <script src="{{ url('frontend/js/vendor/bootstrap.min.js') }}"></script>
        <script src="{{ url('frontend/js/plugins.js') }}"></script>
        <script src="{{ url('frontend/js/app.js') }}"></script>

        <div id="fb-root"></div>
        <script>(function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.11&appId=406147896420973';
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>

    </body>
</html>