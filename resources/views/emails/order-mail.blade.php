<div style="width:100%">
    <h3>ORDER ID: OID.{{ $order_id }}</h3>
    <table>
        <tr>
            <td>
                <h3>BILLING ADDRESS</h3>
                <div style="padding-left: 10px;">
                    <p>Name: {{ Auth::user()->name }}</p>
                    <p>Address: {{ $data_billing->address }}</p>
                    <p>Postal Code: {{ $data_billing->postal_code }}</p>
                    <p>City: {{ $data_billing->city }}</p>
                    <p>Phone: {{ $data_billing->phone }}</p>
                    <p>Email: {{ Auth::user()->email }}</p>
                </div>
            </td>

            <td style="padding-left: 20px;">
                <h3>SHIPPING ADDRESS</h3>
                <div style="padding-left: 10px;">
                    <p>Name: {{ $data_shipping['name'] }}</p>
                    <p>Address: {{ $data_shipping['address'] }}</p>
                    <p>Postal Code: {{ $data_shipping['postal_code'] }}</p>
                    <p>City: {{ $data_shipping['city'] }}</p>
                    <p>Phone: {{ $data_shipping['phone'] }}</p>
                    <p><br></p>
                </div>
            </td>
        </tr>
    </table>
    <h3>ORDER DETAILS</h3>
    <div>
        <table>
            <thead style="background:#cecece">
                <tr>
                    <th style="padding:10px">PRODUCT NAME</th>
                    <th style="padding:10px">DISCRIPTION</th>
                    <th style="padding:10px">QTY</th>
                    <th style="padding:10px">UNIT COST</th>
                    <th style="padding:10px">PRICE</th>                 
                </tr>
            </thead>
            <tbody>
                @foreach(Cart::content() as $item)
                <tr>
                    <td style="padding:10px">{{ $item->name }}</td>
                    <td style="padding:10px">                        
                        <p>Size: {{ $item->options->size }}</p>
                        <p>Color: <span style="background-color: {{ $item->options->color }}; min-width: 20px; display: inline-block;">&nbsp;</span></p>
                    </td>                   
                    <td style="padding:10px">
                        <div>{!! $item->qty !!}</div>
                    </td>
                    <td style="padding:10px">
                        <div>{{ number_format($item->price,0,',','.') }} VND</div>
                    </td>
                    <td style="padding:10px">
                        <div>{!! number_format(($item->price * $item->qty),0,',','.') !!} VND</div>
                    </td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <td style="padding:10px; text-align:right;" colspan="4"><b>Sub Total: </b></td>
                    <td style="padding:10px; text-align:right;">{{ Cart::subtotal(0,',','.') }} VND</td>
                </tr>
                <tr>
                    <td style="padding:10px; text-align:right;" colspan="4"><b>VAT({{ $setting->VAT }}%): </b></td>
                    <td style="padding:10px; text-align:right;">{{ number_format(Cart::subtotal(0,',','')*$setting->VAT/100,0,',','.') }} VND</td>
                </tr>
                <tr>
                    <td style="padding:10px; text-align:right;" colspan="4"><b>Shipping Cost: </b></td>
                    <td style="padding:10px; text-align:right;">{{ number_format($setting->shipping_fee,0,',','.') }} VND</td>
                </tr>
                <tr>
                    <td style="padding:10px; text-align:right;" colspan="4"><b>Total Price: </b></td>
                    <td style="padding:10px; text-align:right;">{{ number_format(Cart::subtotal(0,',','') + Cart::subtotal(0,',','')*$setting->VAT/100 + $setting->shipping_fee ,0,',','.') }} VND</td>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
