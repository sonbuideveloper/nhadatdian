<?php $http_url = env('HTTP_URL'); ?>
<div class="sidebar-content">
    <!-- Brand -->
    <a href="{!! $http_url('admin/dashboard') !!}" class="sidebar-brand">
        <i class="gi gi-flash"></i><span class="sidebar-nav-mini-hide"><strong>SB</strong> ADMIN</span>
    </a>
    <!-- END Brand -->

    <!-- User Info -->
    <div class="sidebar-section sidebar-user clearfix sidebar-nav-mini-hide">
        <div class="sidebar-user-avatar">
            <a href="javascript:void(0)">
                <img src="{!! $http_url((Auth::user()->images == '')? 'backend/img/placeholders/avatars/avatar2.jpg' : getImage(Auth::user()->images)) !!}" alt="avatar">
            </a>
        </div>
        <div class="sidebar-user-name">{!! $user->name !!}</div>
        <div class="sidebar-user-links">
            <a href="{!! $http_url('admin/user/edit/'.Auth::user()->id) !!}" data-toggle="tooltip" data-placement="bottom" title="Profile"><i class="gi gi-user"></i></a>            
            <a href="{!! $http_url('admin/user/logout') !!}" data-toggle="tooltip" data-placement="bottom" title="Logout"><i class="gi gi-exit"></i></a>
        </div>
    </div>
    <!-- END User Info -->

    <!-- Sidebar Navigation -->
    <ul class="sidebar-nav">
        <li>
            <a href="{!! $http_url('admin') !!}" class="@if($active == 'dashboard') active @endif"><i class="gi gi-stopwatch sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Dashboard</span></a>
        </li>

        <li class="sidebar-header">
            <span class="sidebar-header-options clearfix"><a href="javascript:void(0)" data-toggle="tooltip" title="Quick Settings"></a></span>
            <span class="sidebar-header-title">Articles</span>
        </li>
        <li>
            <a href="{!! $http_url('admin/article/list') !!}" class="@if($active == 'article') active @endif"><i class="fa fa-file-text sidebar-nav-icon"></i>All Articles</a>
        </li>
        <li>
            <a href="{!! $http_url('admin/article/create') !!}" class="@if($active == 'article-create') active @endif"><i class="fa fa-plus sidebar-nav-icon"></i>Add Article</a>
        </li>

        {{-- <li>
            <a href="{!! $http_url('admin/home/category') !!}" class="@if($active == 'home_category') active @endif"><i class="gi gi-list sidebar-nav-icon"></i>Navigation</a>
        </li> --}}
        <li class="sidebar-header">
            <span class="sidebar-header-options clearfix"><a href="javascript:void(0)" data-toggle="tooltip" title="Quick Settings"></a></span>
            <span class="sidebar-header-title">General</span>
        </li>
        <li>
            <a href="{!! $http_url('admin/setting/clear-cache') !!}"><i class="gi gi-refresh sidebar-nav-icon"></i> Clear Cache</a>
        </li>
        <li>
            <a href="{!! $http_url('admin/setting/edit') !!}" class="@if($active == 'setting') active @endif"><i class="fa fa-wrench sidebar-nav-icon"></i>Setting</a>
        </li>

    </ul>
    <!-- END Sidebar Navigation -->

    <!-- Sidebar Notifications -->
    <div class="sidebar-header sidebar-nav-mini-hide">
        <span class="sidebar-header-options clearfix">
            <a href="javascript:void(0)" onclick="location.reload();"data-toggle="tooltip" title="Refresh"><i class="gi gi-refresh"></i></a>
        </span>
        <span class="sidebar-header-title">Activity</span>
    </div>
    <!-- END Sidebar Notifications -->
</div>
