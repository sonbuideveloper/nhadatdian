@extends('admin.master')
@section('content')
<?php $http_url = env('HTTP_URL'); ?>
<!-- eCommerce Dashboard Header -->
<div class="content-header">
    <ul class="nav-horizontal text-center">
        <li class="active">
            <a href="{{ $http_url('admin') }}"><i class="fa fa-bar-chart"></i> Dashboard</a>
        </li>
        <li>
            <a href="{{ $http_url('admin/products/category') }}"><i class="fa fa-list"></i> Categories</a>
        </li>  
        {{--<li>--}}
            {{--<a href="{{ $http_url('admin/products/color') }}"><i class="fa fa-adjust"></i> Colors</a>--}}
        {{--</li> --}}
        {{--<li>--}}
            {{--<a href="{{ $http_url('admin/products/orders') }}"><i class="gi gi-shop_window"></i> Orders</a>--}}
        {{--</li>        --}}
        <li>
            <a href="{{ $http_url('admin/products/list') }}"><i class="gi gi-shopping_bag"></i> Products</a>
        </li>
    </ul>
</div>
<!-- END eCommerce Dashboard Header -->

<!-- Quick Stats -->
<div class="row text-center">
        <div class="col-sm-6 col-lg-3">
            <a href="javascript:void(0)" class="widget widget-hover-effect2">
                <div class="widget-extra themed-background">
                    <h4 class="widget-content-light"><strong>Pending</strong> Orders</h4>
                </div>
                <div class="widget-extra-full"><span class="h2 animation-expandOpen">{!! number_format(\Models\Orders::where('publish', 1)->count(), 0, '.', ',') !!}</span></div>
            </a>
        </div>
        <div class="col-sm-6 col-lg-3">
            <a href="javascript:void(0)" class="widget widget-hover-effect2">
                <div class="widget-extra themed-background-dark">
                    <h4 class="widget-content-light"><strong>Orders</strong> Today</h4>
                </div>
                <div class="widget-extra-full"><span class="h2 themed-color-dark animation-expandOpen">{!! number_format(\Models\Orders::whereDay('created_at', date('d'))->count(), 0, '.', ',') !!}</span></div>
            </a>
        </div>
        <div class="col-sm-6 col-lg-3">
            <a href="javascript:void(0)" class="widget widget-hover-effect2">
                <div class="widget-extra themed-background-dark">
                    <h4 class="widget-content-light"><strong>Orders</strong> This Month</h4>
                </div>
                <div class="widget-extra-full"><span class="h2 themed-color-dark animation-expandOpen">{!! number_format(\Models\Orders::whereMonth('created_at', date('m'))->count(), 0, '.',',') !!}</span></div>
            </a>
        </div>
        <div class="col-sm-6 col-lg-3">
            <a href="javascript:void(0)" class="widget widget-hover-effect2">
                <div class="widget-extra themed-background-dark">
                    <h4 class="widget-content-light"><strong>Orders</strong> Last Month</h4>
                </div>
                <div class="widget-extra-full"><span class="h2 themed-color-dark animation-expandOpen">{!! number_format(\Models\Orders::whereMonth('created_at', date('m') - 1)->count(), 0, '.',',') !!}</span></div>
            </a>
        </div>
    </div>
<!-- END Quick Stats -->

<!-- eShop Overview Block -->
{{-- <div class="block full">
    <!-- eShop Overview Title -->
    <div class="block-title">
        <div class="block-options pull-right">
            <div class="btn-group btn-group-sm">
                <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default dropdown-toggle" data-toggle="dropdown">Last Year <span class="caret"></span></a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li class="active">
                        <a href="javascript:void(0)">Last Year</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">Last Month</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">This Month</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">Today</a>
                    </li>
                </ul>
            </div>
            <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="Settings"><i class="fa fa-cog"></i></a>
        </div>
        <h2><strong>eShop</strong> Overview</h2>
    </div>
    <!-- END eShop Overview Title -->

    <!-- eShop Overview Content -->
    <div class="row">
        <div class="col-md-6 col-lg-4">
            <div class="row push">
                <div class="col-xs-6">
                    <h3><strong class="animation-fadeInQuick">45.000</strong><br><small class="text-uppercase animation-fadeInQuickInv"><a href="javascript:void(0)">Total Orders</a></small></h3>
                </div>
                <div class="col-xs-6">
                    <h3><strong class="animation-fadeInQuick">$ 1.200,00</strong><br><small class="text-uppercase animation-fadeInQuickInv"><a href="javascript:void(0)">Cart Value</a></small></h3>
                </div>
                <div class="col-xs-6">
                    <h3><strong class="animation-fadeInQuick">1.520.000</strong><br><small class="text-uppercase animation-fadeInQuickInv"><a href="javascript:void(0)">Visits</a></small></h3>
                </div>
                <div class="col-xs-6">
                    <h3><strong class="animation-fadeInQuick">28.000</strong><br><small class="text-uppercase animation-fadeInQuickInv"><a href="javascript:void(0)">Customers</a></small></h3>
                </div>
                <div class="col-xs-6">
                    <h3><strong class="animation-fadeInQuick">3.5%</strong><br><small class="text-uppercase animation-fadeInQuickInv"><a href="javascript:void(0)">Conv. Rate</a></small></h3>
                </div>
                <div class="col-xs-6">
                    <h3><strong class="animation-fadeInQuick">4.250</strong><br><small class="text-uppercase animation-fadeInQuickInv"><a href="javascript:void(0)">Products</a></small></h3>
                </div>
                <div class="col-xs-6">
                    <h3><strong class="animation-fadeInQuick">$ 260.000,00</strong><br><small class="text-uppercase animation-fadeInQuickInv"><a href="javascript:void(0)">Net Profit</a></small></h3>
                </div>
                <div class="col-xs-6">
                    <h3><strong class="animation-fadeInQuick">$ 16.850,00</strong><br><small class="text-uppercase animation-fadeInQuickInv"><a href="javascript:void(0)">Payment Fees</a></small></h3>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-8">
            <!-- Flot Charts (initialized in js/pages/ecomDashboard.js), for more examples you can check out http://www.flotcharts.org/ -->
            <div id="chart-overview" style="height: 350px;"></div>
        </div>
    </div>
    <!-- END eShop Overview Content -->
</div> --}}
<!-- END eShop Overview Block -->

<!-- Orders and Products -->
<div class="row">
    <div class="col-lg-6">
        <!-- Latest Orders Block -->
        <div class="block">
            <!-- Latest Orders Title -->
            <div class="block-title">
                <div class="block-options pull-right">
                    <a href="{{ $http_url('admin/products/orders') }}" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="Show All"><i class="fa fa-eye"></i></a>                    
                </div>
                <h2><strong>Latest</strong> Orders</h2>
            </div>
            <!-- END Latest Orders Title -->

            <!-- Latest Orders Content -->
            <table class="table table-borderless table-striped table-vcenter table-bordered">
                <tbody>
                    @foreach($lasted_order as $item)
                    <tr>
                        <td class="text-center" style="width: 100px;"><a href="{{ $http_url('admin/products/orders/edit/'.$item->id) }}"><strong>OID.{{ $item->id }}</strong></a></td>
                        <td class="hidden-xs"><a href="{{ $http_url('admin/products/orders/edit/'.$item->id) }}">{{ $item->customerData->name }}</a></td>
                        <td class="hidden-xs">{{ $item->payment_id }}</td>
                        <td class="text-right"><strong>{{ number_format($item->price,0,',','.') }} VND</strong></td>
                        
                        <?php if ($item->publish == 0){
                            echo "<td class='text-left danger'>";
                            echo "<i class='fa fa-refresh fa-spin'></i> Pending ";
                            echo "</td>";
                        }
                        if ($item->publish == 1){                            
                            echo "<td class='text-left info'>";
                            echo "<i class='fa fa-archive'></i> Processing";
                            echo "</td>";
                        }
                        if ($item->publish == 2){
                            echo "<td class='text-left default'>";
                            echo "<i class='fa fa-truck'></i> Shipping";
                            echo "</td>";
                        }
                        if ($item->publish == 3){
                            echo "<td class='text-left success'>";
                            echo "<i class='fa fa-check'></i> Completed";
                            echo "</td>";
                        }
                        if ($item->publish == 4){
                            echo "<td class='text-left danger'>";
                            echo "<i class='fa fa-ban'></i> Canceled";
                            echo "</td>";
                        } ?>
                        
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <!-- END Latest Orders Content -->
        </div>
        <!-- END Latest Orders Block -->
    </div>
    <div class="col-lg-6">
        <!-- Top Products Block -->
        <div class="block">
            <!-- Top Products Title -->
            <div class="block-title">
                <div class="block-options pull-right">
                    <a href="{{ $http_url('admin/products/list') }}" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="Show All"><i class="fa fa-eye"></i></a>                    
                </div>
                <h2><strong>Top</strong> Products</h2>
            </div>
            <!-- END Top Products Title -->

            <!-- Top Products Content -->
            <table class="table table-borderless table-striped table-vcenter table-bordered">
                <tbody>
                    @foreach($top_product as $item)
                    <tr>
                        <td class="text-center" style="width: 100px;"><a href="{{ $http_url('admin/products/edit/'.$item->id) }}"><strong>PID.{{ $item->id }}</strong></a></td>
                        <td><a href="{{ $http_url('admin/products/edit/'.$item->id) }}">{{ $item->name }}</a></td>
                        <td class="text-center"><strong>{{ count($item->orders) }}</strong> orders</td>
                        <td class="hidden-xs text-center">
                        <?php 
                            $total_review = 0;                                                        
                            if(count($item->reviews) > 0){
                                foreach ($item->reviews as $value) {
                                    $total_review += $value->rate;                                                                
                                }
                            }
                            if($total_review){
                                $rate = number_format($total_review/count($item->reviews),1);
                            }else{
                                $rate = 0;
                            }                                                        
                        ?>
                            <div class="text-warning">
                                <i  class="{{ $rate >= 1 ? 'fa fa-star' : 'fa fa-star-o' }}"></i>
                                <i class="{{ $rate >= 2 ? 'fa fa-star' : 'fa fa-star-o' }}"></i>
                                <i class="{{ $rate >= 3 ? 'fa fa-star' : 'fa fa-star-o' }}"></i>
                                <i class="{{ $rate >= 4 ? 'fa fa-star' : 'fa fa-star-o' }}"></i>
                                <i class="{{ $rate >= 5 ? 'fa fa-star' : 'fa fa-star-o' }}"></i>
                            </div>
                        </td>
                    </tr>
                    @endforeach                    
                </tbody>
            </table>
            <!-- END Top Products Content -->
        </div>
        <!-- END Top Products Block -->
    </div>
</div>
<!-- END Orders and Products -->
                    
@endsection