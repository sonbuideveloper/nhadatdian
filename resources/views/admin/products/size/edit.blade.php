@extends('admin.master')
@section('content')
<?php $http_url = env('HTTP_URL'); ?>
    <div class="content-header">
        <div class="header-section">
            <h1><i class="gi gi-pencil"></i> Sizes Product<br><small>Create and Edit Sizes Product</small></h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li>Pages</li>
        <li><a href="{!! $http_url('admin/products/size') !!}">Sizes Product Center</a></li>
        <li>{!! $action !!} Sizes Product</li>
    </ul>
    @if($action == 'Edit')
        <div class="row text-center">
            <div class="col-sm-6 col-lg-3">
                <a href="{!! $http_url('admin/products/size/create') !!}" class="widget widget-hover-effect2">
                    <div class="widget-extra themed-background-success">
                        <h4 class="widget-content-light"><strong>Add New</strong> Sizes Product</h4>
                    </div>
                    <div class="widget-extra-full"><span class="h2 text-success animation-expandOpen"><i class="fa fa-plus"></i></span></div>
                </a>
            </div>
        </div>
    @endif
    @if(Session::get('success'))
        <div class="alert alert-success text-center">
            Success
        </div>
    @endif
    <div class="row">
        <div class="col-lg-12">
            <!-- General Data Block -->

            <div class="block">
                <!-- General Data Title -->
                <div class="block-title">
                    <h2><i class="fa fa-pencil"></i> <strong>General</strong> Data</h2>
                </div>
                <form action="{!! $http_url('admin/products/size/create') !!}" method="post" class="form-horizontal form-bordered">
                    {!! csrf_field() !!}
                    <input type="hidden" name="id" value="{!! (!empty($result))?$result->id:0 !!}">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <ul class="nav nav-tabs" data-toggle="tabs">
                        <li class="active"><a href="#tabs-content-general" data-toggle="tooltip" title="General">General</a></li>
                        @if(!empty($setting) && count($setting) > 0 && $setting->language != null)
                            <?php $language = explode(",", $setting->language);?>
                            @foreach($language as $k)
                                <?php $country = Models\Languages::where('id',$k)->first()?>
                                <li><a href="#tabs-content-{!! $country->language !!}" data-toggle="tooltip" title="{!! $country->language !!}">{!! $country->name !!}</a></li>
                            @endforeach
                        @endif
                    </ul>

                    <div class="tab-content">
                        @if(!empty($setting) && count($setting) > 0 && $setting->language != null)
                            <?php $language = explode(",", $setting->language); $i = 0;?>
                            @foreach($language as $k)
                                <?php $country = Models\Languages::where('id',$k)->first(); ?>
                                <div class="tab-pane" id="tabs-content-{!! $country->language !!}">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="name{!! $country->language !!}">Name</label>
                                        <div class="col-md-9">
                                            <input type="text" id="name{!! $country->language !!}" name="name{!! $country->language !!}" data-id="{!! (!empty($result))?$result->id:0 !!}" data-lang="{!! $country->language !!}" data-url="{!! $http_url('admin/products/size/check-link') !!}" class="form-control" value="{!! (!empty($result) && $result != null && !empty($result->metaData[$i]))?$result->metaData[$i]->name:old('name'.$country->language) !!}" placeholder="Name...">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="slug{!! $country->language !!}">Slug</label>
                                        <div class="col-md-9">
                                            <input type="text" readonly id="slug{!! $country->language !!}" name="slug{!! $country->language !!}" class="form-control" value="{!! (!empty($result) && $result != null && !empty($result->metaData[$i]))?$result->metaData[$i]->slug:old('slug'.$country->language) !!}" placeholder="Slug">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="content{!! $country->language !!}">Content</label>
                                        <div class="col-md-9">
                                            <textarea id="content{!! $country->language !!}" data-id="content{!! $country->language !!}" name="content{!! $country->language !!}" class="ckeditor">{!! (!empty($result) && $result != null && !empty($result->metaData[$i]))?stripslashes($result->metaData[$i]->content):old('content'.$country->language) !!}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <?php $i++;?>
                            @endforeach
                        @endif
                        <div class="tab-pane active" id="tabs-content-general">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="name">Name</label>
                                <div class="col-md-9">
                                    <input type="text" id="name" name="name" data-lang="general" data-id="{!! (!empty($result))?$result->id:0 !!}" data-url="{!! $http_url('admin/products/size/check-link') !!}" class="form-control" value="{!! (!empty($result))?$result->name:old('name') !!}" placeholder="Name...">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="slug">Slug</label>
                                <div class="col-md-9">
                                    <input type="text" id="slug" readonly name="slug" class="form-control" value="{!! (!empty($result))?$result->slug:old('address') !!}" placeholder="Slug">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="content">Content</label>
                                <div class="col-md-9">
                                    <textarea id="content" name="content" class="ckeditor" data-id="content">{!! (!empty($result))?stripslashes($result->content):old('content') !!}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="order_by">Order By</label>
                        <div class="col-md-9">
                            <input type="text" id="order_by" name="order_by" class="form-control" value="{!! (!empty($result))?$result->order_by:old('order_by') !!}" placeholder="Order By">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Published?</label>
                        <div class="col-md-9">
                            <label class="switch switch-primary">
                                <input type="checkbox" id="publish" name="publish" checked><span></span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group form-actions">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
                            <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                        </div>
                    </div>
                <!-- END General Data Content -->

                </form>
            </div>
            <!-- END General Data Block -->
        </div>

    </div>
@endsection
