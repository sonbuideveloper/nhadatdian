<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Models\Contact;
use Models\Setting;

class ContactInfo extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The order instance.
     *
     * @var Contact
     */
    protected $contact;
    protected $setting;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Contact $contact, Setting $setting)
    {
        $this->contact = $contact;
        $this->setting = $setting;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->setting->email, $this->setting->name)
                    ->view('emails.contact-info')
                    ->with([
                        'name' => $this->contact->name
                    ]);
    }
}
