<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Models\UsersShipping;
use Models\UsersData;
use Models\Setting;

class OrderMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $setting;
    protected $data_billing;
    protected $data_shipping;
    protected $order_id;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($setting, $data_billing, $data_shipping, $order_id)
    {
        $this->setting = $setting;
        $this->data_billing = $data_billing;
        $this->data_shipping = $data_shipping;
        $this->order_id = $order_id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->setting->email, $this->setting->name)
                    ->view('emails.order-mail')
                    ->with([
                        'data_billing' => $this->data_billing,
                        'data_shipping' => $this->data_shipping,
                        'order_id'      => $this->order_id,
                    	'setting'		=> $this->setting
                    ]);
    }
}
