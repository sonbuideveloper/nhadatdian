<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

use App\Http\Requests;
use Models\CategoryProducts;
use Models\MasterModel;
use Models\Users;
use Models\Languages;
use Models\Setting;
use Models\Category;
use Redirect, Input, Auth, Session, Validator, Storage;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('language');
    }

    public function getActive(Request $request)
    {
        $token = $request->get('token');
        $check = Users::where(['active_token' => $token])->first();
        if($check != null) {
            MasterModel::CreateContent("users",['active_token' => null, 'active' => 1],['id' => $check->id] );
        }
        return redirect('/')->with([
            'flash_level'   =>  'success',
            'flash_message' =>  trans('message.flash._user-active')
        ]);
    }

    public function postLogin(){
    	$lang = App::getLocale();
        $email = Input::get('login-email', false);
        $password = Input::get('login-password');

        if(Auth::attempt(['email' => $email, 'password' => $password], true)){
            if(Auth::user()->active == 0){
                Auth::logout();
                return redirect()->back()->with(['flash_level'=>'danger','flash_message'=>trans('message.flash._error-verify')]);
            }else{
                return redirect($lang=='vn'?'tai-khoan/bang-dieu-khien':'account/dashboard')->with(['flash_level'=>'success','flash_message'=>trans('message.flash._success')]);
            }
        } else {
            return redirect()->back()->with(['flash_level'=>'danger','flash_message'=>trans('message.flash._email-pass-incorrect')]);
        }
    }

    public function postLoginCheckout(){
        $email = Input::get('login-email', false);
        $password = Input::get('login-password');

        if(Auth::attempt(['email' => $email, 'password' => $password], true)){
            if(Auth::user()->active == 0){
                Auth::logout();
                return redirect()->back()->with(['flash_level'=>'danger','flash_message'=>trans('message.flash._error-verify')]);
            }else{
                return redirect()->back();
            }
        } else {
            return redirect()->back()->with(['flash_level'=>'danger','flash_message'=>trans('message.flash._email-pass-incorrect')]);
        }
    }

    public function getLogin(){
        if(Auth::user()){
            return redirect('/');
        }
        $lang = App::getLocale();
        $lang_id = Session::get('lang_id');
        $language_list = Languages::get();
        $setting = Setting::with('metaData')->first();
        $category = Category::where('type','home')->get();
        if($lang=='vn'){
            $meta = array(
                'title'         => $setting->title,
                'description'   => $setting->description,
                'keywords'      => $setting->keywords,
                'images'        => url('public'.Storage::url($setting->meta_images))
            );
        }else{
            foreach ($setting->metaData as $data){
                if($data->language_id == $lang_id){
                    $meta = array(
                        'title'         => $data->title,
                        'description'   => $data->description,
                        'keywords'      => $data->keywords,
                        'images'        => url('public'.Storage::url($setting->meta_images))
                    );
                }
            }
        }

        $menu = CategoryProducts::orderby('order_by')->with('product')->get();

        return view('frontend.pages.login')->with([
            'page'              => 'login',
            'setting'           => $setting,
            'meta'              => $meta,
            'menu'              => $menu,
        	'lang'              => $lang,
        	'lang_id'           => $lang_id,
        	'language_list'     => $language_list,
            'category'          => $category
        ]) ;
    }

    public function getLogout(){
        $lang = $lang = App::getLocale();
        Auth::logout();
        return redirect()->to('/')->with(['flash_level'=>'success','flash_message'=>trans('message.flash._logout')]);;
    }

    public function postRegister(Request $request){
        $rules = array(
            'email' => 'required|email',
            'password'  => 'required|min:6'
        );
        $message = array(
            'email.required'    => 'Error | Please enter the email',
            'email.email'       => 'Error | The email Malformed',
            'password.required' => 'Error | Please enter password',
            'password.min:6'    => 'Error | Password must min 6 characters',
        );
        $validation = Validator::make($request->all(), $rules, $message);
        if($validation->fails()){
            return Redirect::back()->withErrors($validation)->withInput();
        }

        $data = array(
            'email'     => $request->email,
            'password'  => $request->password,
            'firstname' => '',
            'lastname'  => '',
            'type'      => 0,
            'active'    => 0
        );
        $user = Users::CreateUser($data);
        if($user == false) {
            return redirect()->back()->with(['flash_level'=>'danger','flash_message'=>trans('message.flash._error-email-exist')]);
        }else{
            return redirect()->back()->with(['flash_level'=>'success','flash_message'=>trans('message.flash._creat-account')]);
        }
    }

}
