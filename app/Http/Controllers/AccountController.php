<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth, Validator, Hash, Storage;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;
use Models\Languages;
use Models\Setting;
use Models\Menus;
use Models\Category;
use Models\CategoryProducts;
use Models\Orders;
use Models\Users;
use Models\UsersData;
use App\Models\UsersShippingData;

class AccountController extends Controller
{
	public function __construct()
	{
		$this->middleware('language');
	}
	
	public function getDashboard(){
		$lang = App::getLocale();
		if(!Auth::user()){
			return redirect($lang=='vn'?'tai-khoan/dang-nhap':'users/login');
		}
		
        $lang_id = Session::get('lang_id');
        $language_list = Languages::get();
        $setting = Setting::with('metaData')->first();
        $category_product = CategoryProducts::orderby('order_by')->with('product')->get();        
        $category = Category::where('type','home')->where('publish',1)->get();
        $orders = Orders::where('user_id',Auth::user()->id)->with('shippingData')->orderby('created_at','desc')->take(3)->get();
        $billing_data = UsersData::where('user_id',Auth::user()->id)->first();
        $shipping_data = UsersShippingData::where('user_id',Auth::user()->id)->first();
        
        if($lang=='vn'){
            $meta = array(
                'title'         => $setting->title,
                'description'   => $setting->description,
                'keywords'      => $setting->keywords,
                'images'        => url('public'.Storage::url($setting->meta_images))
            );
        }else{
            foreach ($setting->metaData as $data){
                if($data->language_id == $lang_id){
                    $meta = array(
                        'title'         => $data->title,
                        'description'   => $data->description,
                        'keywords'      => $data->keywords,
                        'images'        => url('public'.Storage::url($setting->meta_images))
                    );
                }
            }
        }
        
        return view('frontend.pages.account-dashboard')->with([
            'page'                  => '',
            'setting'               => $setting,
            'meta'                  => $meta,
            'lang'                  => $lang,
            'lang_id'               => $lang_id,
            'language_list'         => $language_list,
            'menu'                  => $category_product,
            'category'              => $category,
            'orders'				=> $orders,
            'shipping_data'			=> $shipping_data,
            'billing_data'			=> $billing_data
        ]);
	} 

	public function getInformation(){
        if(!Auth::user()){
            return redirect('users/login');
        }
        $lang = App::getLocale();
        $lang_id = Session::get('lang_id');
        $language_list = Languages::get();
        $setting = Setting::with('metaData')->first();
        $category_product = CategoryProducts::orderby('order_by')->with('product')->get();        
        $category = Category::where('type','home')->where('publish',1)->get();
        if($lang=='vn'){
            $meta = array(
                'title'         => $setting->title,
                'description'   => $setting->description,
                'keywords'      => $setting->keywords,
                'images'        => url('public'.Storage::url($setting->meta_images))
            );
        }else{
            foreach ($setting->metaData as $data){
                if($data->language_id == $lang_id){
                    $meta = array(
                        'title'         => $data->title,
                        'description'   => $data->description,
                        'keywords'      => $data->keywords,
                        'images'        => url('public'.Storage::url($setting->meta_images))
                    );
                }
            }
        }
        return view('frontend.pages.account-information')->with([
            'page'                  => 'account-information',
            'setting'               => $setting,
            'meta'                  => $meta,
            'lang'                  => $lang,
            'lang_id'               => $lang_id,
            'language_list'         => $language_list,
            'menu'                  => $category_product,
            'category'              => $category,
        ]);
	}   

    public function postInformation(Request $request){
        if(!Auth::user()){
            return redirect('users/login');
        }
        $rules = array(
            'firstname'     => 'required',
            'lastname'      => 'required',
            'email'         => 'required|email',
        );
        $message = array(
            'email.required'    => 'Please enter the email',
            'email.email'       => 'The email Malformed',
            'firstname.required'=> 'Please enter first name',
            'lastname.required' => 'Please enter last name',
        );
        $validator = Validator::make($request->all(), $rules, $message);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $check_email = Users::where('email',$request->email)->where('id','<>',AUth::user()->id)->first();
        if($check_email){
            return redirect()->back()->with(['flash_level'=>'danger','flash_message'=>trans('message.flash._error-email-exist')]);
        }
        $user_data = Users::find(Auth::user()->id);
        $user_data->firstname = $request->firstname;
        $user_data->lastname  = $request->lastname;
        $user_data->email = $request->email;
        $user_data->name = $request->firstname.' '.$request->lastname;

        if($request->password){  
            if(!$request->new_password){
                return redirect()->back()->with(['flash_level'=>'danger','flash_message'=>trans('message.flash._error-newpass')]);
            }                     
            if (!Hash::check($request->password, $user_data->password)) {
               return redirect()->back()->with(['flash_level'=>'danger','flash_message'=>trans('message.flash._error-oldpass')]);
            }else{
                $user_data->password = bcrypt($request->new_password);
            }
        }

        $file = $request->file('avatar');
        if ($file) {
            $name = $file->getClientOriginalName();
            $name = getNameImage($name);
            $extension = $file->getClientOriginalExtension();
            $fileName = $name.'-'.time().'.'.$extension;

            
            if(Auth::user()->images != null){
                deleteImage(Auth::user()->images);
            }
            $path = putUploadImage($file, $fileName);
            $user_data->images = $path;
        }

        $user_data->save();
        return redirect()->back()->with(['flash_level'=>'success','flash_message'=>trans('message.flash._change-info')]);
    }

	public function getAddressBook(){
        if(!Auth::user()){
            return redirect('users/login');
        }
        $lang = App::getLocale();
        $lang_id = Session::get('lang_id');
        $language_list = Languages::get();
        $setting = Setting::with('metaData')->first();
        $category_product = CategoryProducts::orderby('order_by')->with('product')->get();        
        $category = Category::where('type','home')->where('publish',1)->get();
        $billing_data = UsersData::where('user_id',Auth::user()->id)->first();
        $shipping_data = UsersShippingData::where('user_id',Auth::user()->id)->first();
        
        if($lang=='vn'){
            $meta = array(
                'title'         => $setting->title,
                'description'   => $setting->description,
                'keywords'      => $setting->keywords,
                'images'        => url('public'.Storage::url($setting->meta_images))
            );
        }else{
            foreach ($setting->metaData as $data){
                if($data->language_id == $lang_id){
                    $meta = array(
                        'title'         => $data->title,
                        'description'   => $data->description,
                        'keywords'      => $data->keywords,
                        'images'        => url('public'.Storage::url($setting->meta_images))
                    );
                }
            }
        }
        
        return view('frontend.pages.account-address')->with([
            'page'                  => '',
            'setting'               => $setting,
            'meta'                  => $meta,
            'lang'                  => $lang,
            'lang_id'               => $lang_id,
            'language_list'         => $language_list,
            'menu'                  => $category_product,
            'category'              => $category,            
            'shipping_data'         => $shipping_data,
            'billing_data'          => $billing_data
        ]);
	}

    public function getAddressEdit($slug){
        if(!Auth::user()){
            return redirect('users/login');
        }
        $lang = App::getLocale();
        $lang_id = Session::get('lang_id');
        $language_list = Languages::get();
        $setting = Setting::with('metaData')->first();
        $category_product = CategoryProducts::orderby('order_by')->with('product')->get();        
        $category = Category::where('type','home')->where('publish',1)->get();
        if($slug == 'billing'){
            $data_data = UsersData::where('user_id',Auth::user()->id)->first();
        }else{
            $data_data = UsersShippingData::where('user_id',Auth::user()->id)->first();
        }
        
        if($lang=='vn'){
            $meta = array(
                'title'         => $setting->title,
                'description'   => $setting->description,
                'keywords'      => $setting->keywords,
                'images'        => url('public'.Storage::url($setting->meta_images))
            );
        }else{
            foreach ($setting->metaData as $data){
                if($data->language_id == $lang_id){
                    $meta = array(
                        'title'         => $data->title,
                        'description'   => $data->description,
                        'keywords'      => $data->keywords,
                        'images'        => url('public'.Storage::url($setting->meta_images))
                    );
                }
            }
        }
        
        return view('frontend.pages.account-address-edit')->with([
            'page'                  => '',
            'setting'               => $setting,
            'meta'                  => $meta,
            'lang'                  => $lang,
            'lang_id'               => $lang_id,
            'language_list'         => $language_list,
            'menu'                  => $category_product,
            'category'              => $category,            
            'data'                  => (!empty($data_data)?$data_data:0),
            'slug'                  => $slug
        ]);
    }

    public function postAddressEdit(Request $request){
        if(!Auth::user()){
            return redirect('users/login');
        }

        if($request->slug == 'billing' || $request->slug == 'thanh-toan'){
            $rules = array(
                'phone'         => 'required|digits_between:2,30|numeric',
                'address'       => 'required|max:100',
                'city'          => 'required|max:50',
                'postal_code'   => 'required|digits_between:2,10|numeric'
            );
            $message = array(
                'phone.required'    => 'Please enter the phone',
                'address.required'  => 'Please enter address',
                'city.required'     => 'Please enter city',
                'postal_code.required' => 'Please enter postal code',
            );
            $data = array(
                'phone'         => $request->phone,
                'address'       => $request->address,
                'city'          => $request->city,
                'postal_code'   => $request->postal_code
            );
        }else{
            $rules = array(
                'name'          => 'required',
                'phone'         => 'required|digits_between:2,30|numeric',
                'address'       => 'required|max:100',
                'city'          => 'required|max:50',
                'postal_code'   => 'required|digits_between:2,10|numeric'
            );
            $message = array(
                'name.required'     => 'Please enter the name',
                'phone.required'    => 'Please enter the phone',
                'address.required'  => 'Please enter address',
                'city.required'     => 'Please enter city',
                'postal_code.required' => 'Please enter postal code',
            );
            $data = array(
                'name'          => $request->name,
                'phone'         => $request->phone,
                'address'       => $request->address,
                'city'          => $request->city,
                'postal_code'   => $request->postal_code
            );
        }
        
        $validator = Validator::make($request->all(), $rules, $message);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
		
        if($request->slug == 'billing' || $request->slug == 'thanh-toan'){
            $data = UsersData::where('user_id',Auth::user()->id)->first();
            if($data){                
                $data->phone      = $request->phone;
                $data->address    = $request->address;
                $data->city       = $request->city;
                $data->postal_code= $request->postal_code;
                $data->save();
            }else{
                $data = new UsersData();
                $data->user_id    = Auth::user()->id;
                $data->phone      = $request->phone;
                $data->address    = $request->address;
                $data->city       = $request->city;
                $data->postal_code= $request->postal_code;
                $data->save();
            }
        }else{
            $data = UsersShippingData::where('user_id',Auth::user()->id)->first();
            if($data){
                $data->name       = $request->name;
                $data->phone      = $request->phone;
                $data->address    = $request->address;
                $data->city       = $request->city;
                $data->postal_code= $request->postal_code;
                $data->save();
            }else{
                $data = new UsersShippingData();
                $data->user_id    = Auth::user()->id;
                $data->name       = $request->name;
                $data->phone      = $request->phone;
                $data->address    = $request->address;
                $data->city       = $request->city;
                $data->postal_code= $request->postal_code;
                $data->save();
            }
        }
        return redirect(trans('account.url._account').'/'.trans('account.url._address-book'))->with(['flash_level'=>'success','flash_message'=>trans('message.flash._address-completed')]);
    }

	public function getOrders(){
        if(!Auth::user()){
            return redirect('users/login');
        }
        $lang = App::getLocale();
        $lang_id = Session::get('lang_id');
        $language_list = Languages::get();
        $setting = Setting::with('metaData')->first();
        $category_product = CategoryProducts::orderby('order_by')->with('product')->get();        
        $category = Category::where('type','home')->where('publish',1)->get();
        $orders = Orders::where('user_id',Auth::user()->id)->with('shippingData')->orderby('created_at','desc')->get();
        
        if($lang=='vn'){
            $meta = array(
                'title'         => $setting->title,
                'description'   => $setting->description,
                'keywords'      => $setting->keywords,
                'images'        => url('public'.Storage::url($setting->meta_images))
            );
        }else{
            foreach ($setting->metaData as $data){
                if($data->language_id == $lang_id){
                    $meta = array(
                        'title'         => $data->title,
                        'description'   => $data->description,
                        'keywords'      => $data->keywords,
                        'images'        => url('public'.Storage::url($setting->meta_images))
                    );
                }
            }
        }
        
        return view('frontend.pages.account-orders')->with([
            'page'                  => '',
            'setting'               => $setting,
            'meta'                  => $meta,
            'lang'                  => $lang,
            'lang_id'               => $lang_id,
            'language_list'         => $language_list,
            'menu'                  => $category_product,
            'category'              => $category,
            'orders'                => $orders,
        ]);
	}

	public function getOrderDetail($id){
		if(!Auth::user()){
            return redirect('users/login');
        }
        $lang = App::getLocale();
        $lang_id = Session::get('lang_id');
        $language_list = Languages::get();
        $setting = Setting::with('metaData')->first();
        $category_product = CategoryProducts::orderby('order_by')->with('product')->get();        
        $category = Category::where('type','home')->where('publish',1)->get();
        $order = Orders::where('id',$id)->with('shippingData')->with('userData')->with('orderDetail')->first();
        
        if($lang=='vn'){
            $meta = array(
                'title'         => $setting->title,
                'description'   => $setting->description,
                'keywords'      => $setting->keywords,
                'images'        => url('public'.Storage::url($setting->meta_images))
            );
        }else{
            foreach ($setting->metaData as $data){
                if($data->language_id == $lang_id){
                    $meta = array(
                        'title'         => $data->title,
                        'description'   => $data->description,
                        'keywords'      => $data->keywords,
                        'images'        => url('public'.Storage::url($setting->meta_images))
                    );
                }
            }
        }
        
        return view('frontend.pages.account-order')->with([
            'page'                  => '',
            'setting'               => $setting,
            'meta'                  => $meta,
            'lang'                  => $lang,
            'lang_id'               => $lang_id,
            'language_list'         => $language_list,
            'menu'                  => $category_product,
            'category'              => $category,
            'order'                 => $order,
        ]);
	}
}
