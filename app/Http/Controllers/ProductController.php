<?php

namespace App\Http\Controllers;

use App\Mail\OrderMail;
use App\Models\ProductReview;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Models\CategoryProducts;
use Models\Languages;
use Models\Menus;
use Models\OrderDetail;
use Models\Orders;
use Models\Products;
use Models\Setting;
use App\Models\TagsProduct;
use Auth, Redirect, Validator, Storage;
use Models\UsersShipping;
use Models\Users;
use Models\UsersData;
use Models\MasterModel;
use Models\Category;
use Models\Article;
use App\Models\SizeProduct;
use App\Models\UsersShippingData;

class ProductController extends Controller
{
    public function getProduct($id){
        $lang = App::getLocale();
        $lang_id = Session::get('lang_id');
        $language_list = Languages::get();
        $setting = Setting::with('metaData')->first();
        $menus = Menus::where('publish',1)->orderby('order_by')->get();
        $doi_tac = Article::where('slug','doi-tac')->with('mediaData')->first();
        $latest_products = Products::where('publish',1)->where('featured',1)->with('categories')->orderby('created_at','desc')->take(4)->get();
        $category_product = CategoryProducts::where('publish',1)->orderby('order_by')->with('product')->with('submenu')->get();

//        $category = Category::where('type','home')->where('publish',1)->get();
        $product = Products::where('id', $id)->with('mediaData')->with('categories')->with('reviews')->with('metaData')->first();

        if(Auth::user()){
            $review = ProductReview::where('product_id',$id)->where('user_id',Auth::user()->id)->with('user')->first();
        }else{
            $review = '';
        }
        if($lang=='vn'){
            $meta = array(
                'title'         => (!empty($product->title) ? $product->title : $setting->title),
                'description'   => (!empty($product->descriptions) ? $product->descriptions : $setting->description),
                'keywords'      => (!empty($product->keywords) ? $product->keywords : $setting->keywords),
                'images'        => (!empty($product->images) ? url(Storage::url($product->images)) : url('public'.Storage::url($setting->meta_images)))
            );
        }else{
            if(count($product->metaData) > 0){
                foreach ($product->metaData as $data){
                    if($data->language_id == $lang_id){
                        $meta = array(
                            'title'         => $data->title,
                            'description'   => $data->descriptions,
                            'keywords'      => $data->keywords,
                            'images'        => (!empty($product->images) ? url(Storage::url($product->images)) : url('public'.Storage::url($setting->meta_images)))
                        );
                    }
                }
            }else{
                foreach ($setting->metaData as $data){
                    if($data->language_id == $lang_id){
                        $meta = array(
                            'title'         => $data->title,
                            'description'   => $data->description,
                            'keywords'      => $data->keywords,
                            'images'        => url(Storage::url($setting->meta_images))
                        );
                    }
                }
            }
        }
        $total_review = 0;
        $count_rate['1'] = 0;
        $count_rate['2'] = 0;
        $count_rate['3'] = 0;
        $count_rate['4'] = 0;
        $count_rate['5'] = 0;
        if(count($product->reviews) > 0){
            foreach ($product->reviews as $item) {
                $total_review += $item->rate;
                switch ($item->rate) {
                    case 1:
                        $count_rate['1'] ++;
                        break;
                    case 2:
                        $count_rate['2'] ++;
                        break;
                    case 3:
                        $count_rate['3'] ++;
                        break;
                    case 4:
                        $count_rate['4'] ++;
                        break;
                    case 5:
                        $count_rate['5'] ++;
                        break;
                    default:
                        break;
                }
            }
        }
        if($total_review){
            $rate = number_format($total_review/count($product->reviews),1);
        }else{
            $rate = 0;
        }

        return view('frontend.pages.product')->with([
            'page'              => 'product',
            'setting'           => $setting,
            'meta'              => $meta,
            'lang'              => $lang,
            'lang_id'           => $lang_id,
            'language_list'     => $language_list,
            'menus'             => $menus,
            'doi_tac'           => $doi_tac->mediaData,
            'latest_products'   => $latest_products,
            'category_product'  => $category_product,



//            'category'          => $category,
            'product'           => $product,
//            'tag_product'       => $tag_product,
//            'new_product'       => $new_product,
            'review'            => $review,
            'rate'              => $rate,
            'count_rate'        => $count_rate
        ]) ;
    }

    public function postAddCart(Request $request){
        // dd($request->all());
        $setting = Setting::getSetting();
        $id = $request->get('id');
        if($id){
            $product = Products::where('id',$id)->with('mediaData')->first();
            if(count($product) > 0){
                $data = array(
                    'id'        =>  $id,
                    'name'      =>  $product->name,
                    'qty'       =>  $request->get('txtQty'),
                    'price'     =>  str_replace(',', '', $product->price),
                    'options'   =>  array(
                        'img'       => $product->mediaData[0]->images,
                        'slug'      => $product->slug,
                        'color'     => $request->get('color'),
                        'size'     => $request->get('rdSize'),
                    )
                );
                Cart::add($data);
                return redirect()->back()->with(['flash_level'=>'success','flash_message'=>trans('message.flash._add-cart-success')]);
            }
        }else{
            return redirect()->back()->with(['flash_level'=>'danger','flash_message'=>trans('message.flash._error')]);
        }
    }

    public function getCart(){
        $lang = App::getLocale();
        $lang_id = Session::get('lang_id');
        $language_list = Languages::get();
        $setting = Setting::with('metaData')->first();
        $category = Category::where('type','home')->where('publish',1)->get();
        if($lang=='vn'){
            $meta = array(
                'title'         => $setting->title,
                'description'   => $setting->description,
                'keywords'      => $setting->keywords,
                'images'        => url('public'.Storage::url($setting->meta_images))
            );
        }else{
            foreach ($setting->metaData as $data){
                if($data->language_id == $lang_id){
                    $meta = array(
                        'title'         => $data->title,
                        'description'   => $data->description,
                        'keywords'      => $data->keywords,
                        'images'        => url('public'.Storage::url($setting->meta_images))
                    );
                }
            }
        }
        $category_product = CategoryProducts::where('publish',1)->orderby('order_by')->with('product')->with('submenu')->get();

        return view('frontend.pages.cart')->with([
            'page'              => 'cart',
            'setting'           => $setting,
            'meta'              => $meta,
            'lang'              => $lang,
            'lang_id'           => $lang_id,
            'language_list'     => $language_list,
            'menu'              => $category_product,
            'category'          => $category,
            'product'           => Cart::content()
        ]) ;
    }

    public function getClearCart(){
        Cart::destroy();
        return redirect()->back();
    }

    public function getCartDelete($id){
        if(isset($id)){
            Cart::remove($id);
            return redirect()->back();
        }
    }

    public function postCartUpdate(){
        $qty = $_POST['qty'];
        $rowId = $_POST['rowId'];
        if(isset($rowId) && $qty > 0){
            Cart::update($rowId, ['qty' => $qty]);
            echo 'success';
        }
    }

    public function getCheckOut(){
    	$lang = App::getLocale();
    	if(Auth::user()){
            return redirect($lang=='vn'?'thong-tin-ca-nhan':'personal-info');
        }

        $lang_id = Session::get('lang_id');
        $language_list = Languages::get();
        $setting = Setting::with('metaData')->first();
        $category = Category::where('type','home')->where('publish',1)->get();
        if($lang=='vn'){
            $meta = array(
                'title'         => $setting->title,
                'description'   => $setting->description,
                'keywords'      => $setting->keywords,
                'images'        => url('public'.Storage::url($setting->meta_images))
            );
        }else{
            foreach ($setting->metaData as $data){
                if($data->language_id == $lang_id){
                    $meta = array(
                        'title'         => $data->title,
                        'description'   => $data->description,
                        'keywords'      => $data->keywords,
                        'images'        => url('public'.Storage::url($setting->meta_images))
                    );
                }
            }
        }
        $category_product = CategoryProducts::where('publish',1)->orderby('order_by')->with('product')->with('submenu')->get();

        return view('frontend.pages.checkout')->with([
        	'page'              => 'checkout',
        	'setting'           => $setting,
        	'meta'              => $meta,
        	'lang'              => $lang,
        	'lang_id'           => $lang_id,
        	'language_list'     => $language_list,
            'menu'              => $category_product,
            'category'          => $category
        ]) ;
    }

    public function getPersonalInfo(){
    	$lang = App::getLocale();
        if(!Auth::user()){
            return redirect($lang=='vn'?'tai-khoan/dang-nhap':'users/login');
        }
        $lang_id = Session::get('lang_id');
        $language_list = Languages::get();
        $setting = Setting::with('metaData')->first();
        $category = Category::where('type','home')->where('publish',1)->get();
        if($lang=='vn'){
            $meta = array(
                'title'         => $setting->title,
                'description'   => $setting->description,
                'keywords'      => $setting->keywords,
                'images'        => url('public'.Storage::url($setting->meta_images))
            );
        }else{
            foreach ($setting->metaData as $data){
                if($data->language_id == $lang_id){
                    $meta = array(
                        'title'         => $data->title,
                        'description'   => $data->description,
                        'keywords'      => $data->keywords,
                        'images'        => url('public'.Storage::url($setting->meta_images))
                    );
                }
            }
        }
        $category_product = CategoryProducts::where('publish',1)->orderby('order_by')->with('product')->with('submenu')->get();
        $billing_data = Session::get('billing_data');

        return view('frontend.pages.personal-info')->with([
            'page'              => 'personal-info',
            'setting'           => $setting,
            'meta'              => $meta,
        	'lang'              => $lang,
        	'lang_id'           => $lang_id,
        	'language_list'     => $language_list,
            'menu'              => $category_product,
            'category'          => $category,
            'billing_data'      => $billing_data
        ]) ;
    }

    public function postPersonalInfo(Request $request){
        $user_id = $request->get('txtUserID');
        $rules = array(
            'firstname'      => 'required',
            'lastname'      => 'required',
        );
        $validation = Validator::make($request->all(), $rules);
        if($validation->fails()){
            return Redirect::back()->withErrors($validation)->withInput($request->all());
        }
        $lang = App::getLocale();
        if($user_id){
            $user = Users::find($user_id);
            if($user){
                $user->firstname = $request->get('firstname');
                $user->lastname = $request->get('lastname');
                $user->name = $user->firstname.' '.$user->lastname;
                $user->save();
                return redirect($lang=='vn'?'dia-chi':'billing');
            }else{
                return redirect()->back();
            }
        }else{
            return redirect()->back();
        }
    }

    public function getBilling(){
        if(!Auth::user()){
        	return redirect($lang=='vn'?'tai-khoan/dang-nhap':'users/login');
        }
        $lang = App::getLocale();
        $lang_id = Session::get('lang_id');
        $language_list = Languages::get();
        $setting = Setting::with('metaData')->first();
        $category = Category::where('type','home')->where('publish',1)->get();
        if($lang=='vn'){
            $meta = array(
                'title'         => $setting->title,
                'description'   => $setting->description,
                'keywords'      => $setting->keywords,
                'images'        => url('public'.Storage::url($setting->meta_images))
            );
        }else{
            foreach ($setting->metaData as $data){
                if($data->language_id == $lang_id){
                    $meta = array(
                        'title'         => $data->title,
                        'description'   => $data->description,
                        'keywords'      => $data->keywords,
                        'images'        => url('public'.Storage::url($setting->meta_images))
                    );
                }
            }
        }
        $category_product = CategoryProducts::where('publish',1)->orderby('order_by')->with('product')->with('submenu')->get();
        $user = Users::where('id',Auth::user()->id)->with('userData')->with('shippingData')->first();

        if($user->userData){
            $billing_data = $user->userData;
        }else{
            $billing_data = '';
        }

        if($user->shippingData){
            $shipping_data = $user->shippingData;
        }else{
            $shipping_data = '';
        }

        return view('frontend.pages.billing')->with([
            'page'              => 'billing',
            'setting'           => $setting,
            'meta'              => $meta,
        	'lang'              => $lang,
        	'lang_id'           => $lang_id,
        	'language_list'     => $language_list,
            'menu'              => $category_product,
            'billing_data'      => $billing_data,
            'shipping_data'     => $shipping_data,
            'category'          => $category,
        ]) ;
    }

    public function postBilling(Request $request){
        $user_id = $request->get('txtUserID');
        $lang = App::getLocale();
        $billing = UsersData::where('user_id',$user_id)->first();
        if($billing){
            $billing_data = array(
                'address'       => $request->get('billing-address'),
                'city'          => $request->get('billing-city'),
                'phone'         => $request->get('billing-phone'),
                'postal_code'   => $request->get('billing-postal'),
            );
            MasterModel::CreateContent('users_data', $billing_data, ['user_id' => $user_id]);
        }else{
            $billing_data = array(
                'user_id'       => $user_id,
                'address'       => $request->get('billing-address'),
                'city'          => $request->get('billing-city'),
                'phone'         => $request->get('billing-phone'),
                'postal_code'   => $request->get('billing-postal'),
            );
            $billing_id = MasterModel::CreateContent('users_data', $billing_data);
        }

        $shipping = UsersShippingData::where('user_id',$user_id)->first();
        if($shipping){
            $shipping_data = array(
                'name'          => $request->get('shipping-name'),
                'address'       => $request->get('shipping-address'),
                'city'          => $request->get('shipping-city'),
                'phone'         => $request->get('shipping-phone'),
                'postal_code'   => $request->get('shipping-postal'),
            );
            MasterModel::CreateContent('users_shipping_datas', $shipping_data, ['user_id' => $user_id]);
        }else{
            $shipping_data = array(
                'user_id'       => $user_id,
                'name'          => $request->get('shipping-name'),
                'address'       => $request->get('shipping-address'),
                'city'          => $request->get('shipping-city'),
                'phone'         => $request->get('shipping-phone'),
                'postal_code'   => $request->get('shipping-postal'),
            );
            $shipping_id = MasterModel::CreateContent('users_shipping_datas', $shipping_data);
        }
        return redirect($lang=='vn'?'thanh-toan':'payment');
    }

    public function getPayment(){
    	$lang = App::getLocale();
    	if(!Auth::user()){
        	return redirect($lang=='vn'?'tai-khoan/dang-nhap':'users/login');
        }
        $lang_id = Session::get('lang_id');
        $language_list = Languages::get();
        $setting = Setting::with('metaData')->first();
        $category = Category::where('type','home')->where('publish',1)->get();
        if($lang=='vn'){
            $meta = array(
                'title'         => $setting->title,
                'description'   => $setting->description,
                'keywords'      => $setting->keywords,
                'images'        => url('public'.Storage::url($setting->meta_images))
            );
        }else{
            foreach ($setting->metaData as $data){
                if($data->language_id == $lang_id){
                    $meta = array(
                        'title'         => $data->title,
                        'description'   => $data->description,
                        'keywords'      => $data->keywords,
                        'images'        => url('public'.Storage::url($setting->meta_images))
                    );
                }
            }
        }
        $category_product = CategoryProducts::where('publish',1)->orderby('order_by')->with('product')->with('submenu')->get();

        return view('frontend.pages.payment')->with([
            'page'              => 'payment',
            'setting'           => $setting,
            'meta'              => $meta,
        	'lang'              => $lang,
        	'lang_id'           => $lang_id,
        	'language_list'     => $language_list,
            'menu'              => $category_product,
            'category'          => $category,
        ]) ;
    }

    public function postPayment(Request $request){
    	$lang = App::getLocale();
        if($request->get('payments') == 'cash'){
            return redirect($lang=='vn'?'xac-nhan-don-hang':'confirm-order');
        }elseif($request->get('payments') == 'paypal'){
            dd('paypal');
        }else{
            return redirect()->back()->with(['flash_level'=>'danger','flash_message'=>trans('message.flash._error')]);
        }
    }

    public function getConfirmOrder(){
    	$lang = App::getLocale();
        if(!Auth::user()){
        	return redirect($lang=='vn'?'tai-khoan/dang-nhap':'users/login');
        }
        $lang_id = Session::get('lang_id');
        $language_list = Languages::get();
        $setting = Setting::with('metaData')->first();
        $user = Users::where('id',Auth::user()->id)->with('userData')->with('shippingData')->first();
        $category = Category::where('type','home')->where('publish',1)->get();
        if($lang=='vn'){
            $meta = array(
                'title'         => $setting->title,
                'description'   => $setting->description,
                'keywords'      => $setting->keywords,
                'images'        => url('public'.Storage::url($setting->meta_images))
            );
        }else{
            foreach ($setting->metaData as $data){
                if($data->language_id == $lang_id){
                    $meta = array(
                        'title'         => $data->title,
                        'description'   => $data->description,
                        'keywords'      => $data->keywords,
                        'images'        => url('public'.Storage::url($setting->meta_images))
                    );
                }
            }
        }
        $category_product = CategoryProducts::where('publish',1)->orderby('order_by')->with('product')->with('submenu')->get();

        return view('frontend.pages.confirm-order')->with([
            'page'              => 'confirm-order',
            'setting'           => $setting,
            'meta'              => $meta,
            'lang'              => $lang,
            'lang_id'           => $lang_id,
            'language_list'     => $language_list,
            'menu'              => $category_product,
            'category'          => $category,
            'product'           => Cart::content(),
            'billing_data'      => $user->userData,
            'shipping_data'     => $user->shippingData
        ]) ;
    }

    public function postConfirmOrder(Request $request){
        $user_id = $request->get('txtUserID');
        $setting = Setting::first();
        $data_order = array(
            'user_id'       => $user_id,
            'payment_id'    => 'cash',
            'qty'           => Cart::count(),
            'price'         => str_replace('.','',str_replace(' VND','',$request->get('total_price'))),
            'publish'       => 1
        );
        $user = Users::where('id',$user_id)->with('userData')->with('shippingData')->first();
        $data_billing = $user->userData;
        $data_shipping = $user->shippingData;

        $order_id = Orders::addNewOrder($data_order);
        if($order_id){
            UsersShipping::createShippingData($order_id, $user_id, $data_shipping);
            OrderDetail::CreateData($order_id, Cart::content());
            Mail::to($user->email)->send(new OrderMail($setting, $data_billing ,$data_shipping, $order_id));
            // Mail::to($setting->email)->send(new OrderMail($setting, $data_billing ,$data_shipping[0], $order_id));
            Cart::destroy();
            return redirect('/')->with(['flash_level'=>'success','flash_message'=>trans('message.flash._order-send')]);
        }else{
            return redirect()->back()->with(['flash_level'=>'danger','flash_message'=>trans('message.flash._error')]);
        }
    }

    public function postReview(Request $request){
        if(!Auth::user()){
        	return redirect($lang=='vn'?'tai-khoan/dang-nhap':'users/login');
        }
        $user_id = Auth::user()->id;
        $rules = array(
            'product_id'    => 'required',
            'message'       => 'required',
            'rating'        => 'required'
        );
        $validation = Validator::make($request->all(), $rules);
        if($validation->fails()){
            return Redirect::back()->withErrors($validation)->withInput($request->all());
        }
        $review_check = ProductReview::where('user_id',$user_id)->where('product_id',$request->get('product_id'))->first();
        if($review_check){
            $review_check->rate = $request->get('rating');
            $review_check->message = $request->get('message');
            $review_check->save();
        }else{
            $review = new ProductReview();
            $review->user_id = $user_id;
            $review->product_id = $request->get('product_id');
            $review->rate = $request->get('rating');
            $review->message = $request->get('message');
            $review->save();
        }
        return redirect()->back();

    }


}
