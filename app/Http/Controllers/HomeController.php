<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Admin\SettingController;
use App\Mail\ContactInfo;
use App\Mail\SendContactAdmin;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Lib\Localization;
use Models\CategoryProducts;
use Models\Contact;
use Models\Languages;
use Models\Products;
use Validator;
use Models\Article;
use Models\Category;
use Models\Menus;
use Models\Setting;
use Mcamara\LaravelLocalization\LaravelLocalization;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('language');
    }

    public function Language(){
        if(isset($_POST['lang'])){
            $lang = $_POST['lang'];
            $url = $_POST['url'];

            $langguage = new Localization();
            $url = $langguage->getLanguageSwitchUrl($lang,$url);
            echo url($lang);
        }else{
            echo 'fail';
        }
    }

}
