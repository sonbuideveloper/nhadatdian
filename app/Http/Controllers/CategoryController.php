<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;
use Models\Article;
use Models\Banner;
use Models\CategoryProducts;
use App\Models\ColorProduct;
use App\Models\TagsProduct;
use App\Models\SizeProduct;
use Models\Contents;
use Models\Languages;
use Models\Products;
use Models\Setting;
use Models\Menus;
use Models\Category;
use App\Models\CategoryProductRelation;
use App\Models\SizeProductRelation;
use App\Models\ColorProductRelation;
use Validator;
use Storage;
use Cache;

class CategoryController extends Controller
{
    public function index(){
        Session::set('guest_id',$_SERVER['REMOTE_ADDR']);
        self::get_user_online();
        $setting = Setting::with('metaData')->first();
        $menus = Menus::where('publish', 1)->orderby('order_by')->get();
        $menu_home = Menus::where('slug', 'trang-chu')->with('banner')->where('parent_id', 0)->first();
        $menu_article = Menus::where('slug', 'bai-viet')->where('parent_id', 0)->first();
        $articles = Article::where('publish', 1)->where('category_id', $menu_article->id)->with('categories')->orderby('id', 'desc')->paginate(5);
        $featured = Article::where('publish', 1)->where('category_id', $menu_article->id)->where('featured', 1)->orderby('id', 'desc')->take(5)->get();

        $meta = array(
            'title' => $setting->title,
            'description' => $setting->description,
            'keywords' => $setting->keywords,
            'images' => url(Storage::url($setting->meta_images))
        );

        $data_cache = [
            'page' => 'home',
            'setting' => $setting,
            'meta' => $meta,
            'menus' => $menus,
            'banner' => $menu_home->banner[0],
            'articles' => $articles,
            'featured' => $featured
        ];

        return view('frontend.pages.home')->with($data_cache);
    }

    public function getContactUs(){
        $setting = Setting::with('metaData')->first();
        $menus = Menus::where('publish', 1)->orderby('order_by')->get();
        $menu_home = Menus::where('slug', 'trang-chu')->with('banner')->where('parent_id', 0)->first();
        $contact_menu = Menus::where('slug','lien-he')->where('parent_id', 0)->first();
        $contact = Article::where('category_id', $contact_menu->id)->first();

        $meta = array(
            'title' => $setting->title,
            'description' => $setting->description,
            'keywords' => $setting->keywords,
            'images' => url(Storage::url($setting->meta_images))
        );

        $data_cache = [
            'page' => 'contact-us',
            'setting' => $setting,
            'menus' => $menus,
            'meta' => $meta,
            'banner' => $menu_home->banner[0],
            'contact' => (!empty($contact) ? $contact : '')
        ];

        return view('frontend.pages.contact-us')->with($data_cache);
    }

    public function postContactUs(Request $request){
        Session::flash('success','success');
        return redirect()->back();
    }

    public function getArticles($slug){
        $article = Article::where('slug', $slug)->where('publish', 1)->first();
        $setting = Setting::with('metaData')->first();
        $menus = Menus::where('publish', 1)->orderby('order_by')->get();
        $menu_article = Menus::where('slug', 'bai-viet')->where('parent_id', 0)->first();
        $menu_home = Menus::where('slug', 'trang-chu')->with('banner')->where('parent_id', 0)->first();
        $featured = Article::where('publish', 1)->where('category_id', $menu_article->id)->where('featured', 1)->orderby('id', 'desc')->take(5)->get();

        $meta = array(
            'title' => $setting->title,
            'description' => $setting->description,
            'keywords' => $setting->keywords,
            'images' => url(Storage::url($setting->meta_images))
        );

        $data_cache = [
            'page' => 'home',
            'setting' => $setting,
            'meta' => $meta,
            'menus' => $menus,
            'banner' => $menu_home->banner[0],
            'featured' => $featured,
            'article' => $article
        ];

        return view('frontend.pages.detail')->with($data_cache);
    }

    public function getSearch(Request $request){
        $key = $request->k;
        if(!$key){
            return redirect()->back();
        }
        $setting = Setting::with('metaData')->first();
        $menus = Menus::where('publish', 1)->orderby('order_by')->get();
        $menu_home = Menus::where('slug', 'trang-chu')->with('banner')->where('parent_id', 0)->first();
        $menu_article = Menus::where('slug', 'bai-viet')->where('parent_id', 0)->first();
        $articles = Article::where('publish', 1)->where('category_id', $menu_article->id)->where('name','like',"%$key%")->with('categories')->orderby('id', 'desc')->get();
        $featured = Article::where('publish', 1)->where('category_id', $menu_article->id)->where('featured', 1)->orderby('id', 'desc')->take(5)->get();

        $meta = array(
            'title' => $setting->title,
            'description' => $setting->description,
            'keywords' => $setting->keywords,
            'images' => url(Storage::url($setting->meta_images))
        );

        $data_cache = [
            'page' => 'home',
            'setting' => $setting,
            'meta' => $meta,
            'menus' => $menus,
            'banner' => $menu_home->banner[0],
            'articles' => $articles,
            'featured' => $featured,
            'key'   => $key
        ];

        return view('frontend.pages.search')->with($data_cache);
    }

    public static function get_user_online(){
        $count_access = Setting::first();
        $guest_id = Session::get('guest_id');
        $time = time();
        $time_check = $time-600;

        //Nếu quá 10 phút, xóa bỏ session
        \DB::table('guest_online')->where('time','<',$time_check)->delete();
        $sql = \DB::table('guest_online')->where('guest_id', $guest_id)->get();
        if(count($sql)==0){
            \DB::table('guest_online')->insert(['guest_id'=>$guest_id, 'time'=>$time]);
            $count_access->shipping_fee = $count_access->shipping_fee + 1;
            $count_access->save();
        }else{
            \DB::table('guest_online')->where('guest_id',$guest_id)->update(['time'=>$time]);
        }

        return $online = \DB::table('guest_online')->get();
    }
}
