<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsersShippingData extends Model
{
    protected $table = "users_shipping_datas";

    public static function createShippingData($user_id, $data){
        $shipping = new self;
        $shipping->user_id = $user_id;
        $shipping->order_id = $order_id;
        $shipping->address = $data['address'];
        $shipping->name = $data['name'];
        $shipping->phone = $data['phone'];
        $shipping->postal_code = $data['postal_code'];
        $shipping->city = $data['city'];
        $shipping->save();
        return $shipping->id;
    }
}
