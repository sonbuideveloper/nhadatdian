<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SizeProductRelation extends Model
{
    protected $table = "size_product_relations";
}
