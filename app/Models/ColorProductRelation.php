<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ColorProductRelation extends Model
{
    protected $table = "color_product_relations";
}
