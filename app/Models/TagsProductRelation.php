<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TagsProductRelation extends Model
{
    protected $table = "tags_product_relations";
}
