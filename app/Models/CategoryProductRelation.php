<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryProductRelation extends Model
{
    protected $table = "category_product_relations";
}
