<?php

namespace Models;

use Illuminate\Database\Eloquent\Model;

class UsersShipping extends Model
{
    //
    protected $table = "shipping_data";

    public static function createShippingData($order_id, $user_id, $data){
        $shipping = new self;
        $shipping->user_id = $user_id;
        $shipping->order_id = $order_id;
        $shipping->address = $data['address'];
        $shipping->name = $data['name'];
        $shipping->phone = $data['phone'];
        $shipping->postal_code = $data['postal_code'];
        $shipping->city = $data['city'];
        $shipping->save();
        return $shipping->id;
    }
}
