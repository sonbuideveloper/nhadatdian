<?php
namespace Lib;

use Mcamara\LaravelLocalization\LaravelLocalization;
use Illuminate\Support\Facades\Config;

class Localization extends LaravelLocalization
{
    public function getLanguageSwitchUrl($locale,$base_url)
    {
        Config::set('laravellocalization.hideDefaultLocaleInURL', false);
        $url = $this->getLocalizedURL($locale,$base_url);
        Config::set('laravellocalization.hideDefaultLocaleInURL', true);
        return $url;
    }

}
